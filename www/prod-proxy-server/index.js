let http = require('http');
let ws = require('ws');
let proxy = require('http-proxy');
let url = require('url');
const fs = require('fs');
const path = require('path');

const mimeType = {
    '.ico': 'image/x-icon',
    '.html': 'text/html',
    '.js': 'text/javascript',
    '.json': 'application/json',
    '.css': 'text/css',
    '.png': 'image/png',
    '.jpg': 'image/jpeg',
    '.wav': 'audio/wav',
    '.mp3': 'audio/mpeg',
    '.svg': 'image/svg+xml',
    '.pdf': 'application/pdf',
    '.doc': 'application/msword',
    '.eot': 'appliaction/vnd.ms-fontobject',
    '.ttf': 'aplication/font-sfnt'
};

const proxyServer = proxy.createProxyServer({});

const server = http.createServer((request, response) => {
    const pathname = url.parse(request.url).pathname;
    if (pathname.startsWith("/api"))
        proxyServer.web(request, response, {target: 'http://127.0.0.1:7000'});
    else {
        const sanitizePath = path.normalize(url.parse(request.url).pathname).replace(/^(\.\.[\/\\])+/, '');
        let pathname = path.join(__dirname, "static", sanitizePath);
        fs.exists(pathname, function (exist) {
            if (!exist) {
                if (pathname.indexOf('.') === -1) {
                    pathname = path.join(__dirname, "static", path.normalize("/index.html").replace(/^(\.\.[\/\\])+/, ''));
                } else {
                    // if the file is not found, return 404
                    response.statusCode = 404;
                    response.end(`File ${pathname} not found!`);
                    return;
                }
            }
            // if is a directory, then look for index.html
            if (fs.statSync(pathname).isDirectory()) {
                pathname += '/index.html';
            }
            // read file from file system
            fs.readFile(pathname, function (err, data) {
                if (err) {
                    response.statusCode = 500;
                    response.end(`Error getting the file: ${err}.`);
                } else {
                    // based on the URL path, extract the file extention. e.g. .js, .doc, ...
                    const ext = path.parse(pathname).ext;
                    // if the file is found, set Content-type and send data
                    response.setHeader('Content-Type', mimeType[ext] || 'text/plain');
                    response.end(data);
                }
            });
        });
    }

});

const viewWSServerMiddleware = new ws.Server({noServer: true});
const controlWSServerMiddleware = new ws.Server({noServer: true});

let views = [];
viewWSServerMiddleware.on('connection', function connection(ws) {
    views.push(ws);
    ws.on('close', () => views.slice(views.indexOf(ws), 1));
});

controlWSServerMiddleware.on('connection', function connection(ws) {
    ws.on('message', (m) => views.filter(value => value.readyState === WebSocket.OPEN).forEach(value => value.send(m)));
});

server.on('upgrade', function upgrade(request, socket, head) {
    const pathname = url.parse(request.url).pathname;
    console.log(pathname);
    if (pathname === '/api/control/websocket/view') {
        viewWSServerMiddleware.handleUpgrade(request, socket, head, function done(ws) {
            viewWSServerMiddleware.emit('connection', ws, request);
        });
    } else if (pathname === '/api/control/websocket/controller') {
        controlWSServerMiddleware.handleUpgrade(request, socket, head, function done(ws) {
            controlWSServerMiddleware.emit('connection', ws, request);
        });
    } else {
        socket.destroy();
    }
});

server.listen(3333);