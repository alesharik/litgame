var path = require('path');
var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var config = require('./webpack.config');
var compiler = webpack(config);
var http = require('http');
var url = require('url');

const WebSocket = require('ws');

const s = http.createServer();
const wss1 = new WebSocket.Server({noServer: true});
const wss2 = new WebSocket.Server({noServer: true});

let views = [];

wss1.on('connection', function connection(ws) {
    views.push(ws);
    ws.on('close', () => views.slice(views.indexOf(ws), 1));
});

wss2.on('connection', function connection(ws) {
    ws.on('message', (m) => views.filter(value => value.readyState === WebSocket.OPEN).forEach(value => value.send(m)));
});

s.on('upgrade', function upgrade(request, socket, head) {
    const pathname = url.parse(request.url).pathname;
    console.log(pathname);
    if (pathname === '/api/control/websocket/view') {
        wss1.handleUpgrade(request, socket, head, function done(ws) {
            wss1.emit('connection', ws, request);
        });
    } else if (pathname === '/api/control/websocket/controller') {
        wss2.handleUpgrade(request, socket, head, function done(ws) {
            wss2.emit('connection', ws, request);
        });
    } else {
        socket.destroy();
    }
});

s.listen(1122);

var server = new WebpackDevServer(compiler, {
    hot: true,
    // display no info to console (only warnings and errors)
    noInfo: false,
    publicPath: "/",
    stats: {
        // With console colors
        colors: true,
        // add the hash of the compilation
        hash: true,
        // add webpack version information
        version: false,
        // add timing information
        timings: true,
        // add assets information
        assets: false,
        // add chunk information
        chunks: false,
        // add built modules information to chunk information
        chunkModules: false,
        // add built modules information
        modules: false,
        // add also information about cached (not built) modules
        cached: false,
        // add information about the reasons why modules are included
        reasons: false,
        // add the source code of modules
        source: false,
        // add details to errors (like resolving log)
        errorDetails: true,
        // add the origins of chunks and chunk merging info
        chunkOrigins: false,
        // Add messages from child loaders
        children: false
    },
    disableHostCheck: true,
    historyApiFallback: true,
    proxy: {
        '/api/control/websocket': {
            target: 'http://localhost:1122/',
            ws: true
        },
        '/api': {
            target: 'http://localhost:7000',
            ws: false
        }
    }
});

server.listen(3400, '0.0.0.0', function (err) {
    if (err) {
        console.log(err);
        return;
    }

    console.log("Listening at http://localhost:3333/litgame/. Please wait, I'm building things for you...");
});
