import * as React from "react";
import {Button, Modal} from "semantic-ui-react";
import ErrorHandler from "./ErrorHandler";

class ErrorHandlerModalState {
    open: boolean;
    status: string;

    constructor(open: boolean, status: string) {
        this.open = open;
        this.status = status;
    }
}

export class ErrorHandlerModal extends React.Component<{}, ErrorHandlerModalState> {
    constructor(props: {}, context: any) {
        super(props, context);
        this.state = new ErrorHandlerModalState(false, "");
        this.handleError = this.handleError.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.componentWillUnmount = this.componentWillUnmount.bind(this);
    }

    componentDidMount(): void {
        super.componentDidMount();
        ErrorHandler.register(this.handleError);
    }

    componentWillUnmount(): void {
        ErrorHandler.unregister(this.handleError);
        super.componentWillUnmount();
    }

    handleError(reason: string) {
        this.setState(new ErrorHandlerModalState(true, reason));
    }

    render() {
        return (
            <Modal basic={true}>
                <Modal.Header><p>ОШИБКА</p></Modal.Header>
                <Modal.Content>
                    <p>{this.state.status}</p>
                </Modal.Content>
                <Modal.Actions>
                    <Button color={"red"}
                            onClick={() => this.setState(new ErrorHandlerModalState(false, ""))}>Закрыть</Button>
                    <Button color={"green"} onClick={() => location.reload()}>Перезагрузить страницу</Button>
                </Modal.Actions>
            </Modal>
        );
    }
}