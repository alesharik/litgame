let _error_handlers: Array<(reason: string) => void> = [];

export default class ErrorHandler {
    static handleHttpError(reason: any) {
        console.error(reason);
        _error_handlers.forEach(value => value(reason));
    }

    static register(h: (reason: string) => void) {
        _error_handlers.push(h);
    }

    static unregister(h: (reason: string) => void) {
        _error_handlers.slice(_error_handlers.indexOf(h), 1);
    }
}