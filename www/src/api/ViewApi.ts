export class ViewApi {
    static getImageUrl(id: string) {
        return "/api/control/image/data?id=" + id;
    }
}