import {Theme, ThemeEditApi} from "./ThemeEditApi";

export class Image {
    public id: string;
    public name: string;
    public theme: Theme;
    public answer: boolean;
    public question: boolean;

    constructor(id: string, name: string, theme: Theme, answer: boolean, question: boolean) {
        this.id = id;
        this.name = name;
        this.theme = theme;
        this.answer = answer;
        this.question = question;
    }
}

class _Image {
    public id: string;
    public name: string;
    public theme: string;
    public answer: boolean;
    public question: boolean;

    constructor(id: string, name: string, theme: string, answer: boolean, question: boolean) {
        this.id = id;
        this.name = name;
        this.theme = theme;
        this.answer = answer;
        this.question = question;
    }
}

class ImageApiRequestInit implements RequestInit {
    public body: string;
    public method: string;
    public headers: Headers;

    constructor(method: string, body?: object) {
        this.method = method;
        if (body != undefined) {
            this.body = JSON.stringify(body);
            this.headers = new Headers();
            this.headers.append("Content-Type", "application/json; charset=UTF-8");
        }
    }
}

class LoadImageRequestInit implements RequestInit {
    public body: Blob;
    public method: string;

    constructor(body: Blob) {
        this.method = "PUT";
        this.body = body;
    }
}

export class ImageEditApi {
    public static selectImages(theme: Theme): Promise<Array<Image>> {
        return new Promise<Array<Image>>((resolve, reject) => {
            fetch("/api/edit/image/select?theme_id=" + theme.id)
                .then(value => value.json())
                .then(value => {
                    // noinspection UnnecessaryLocalVariableJS
                    let images: Array<_Image> = value;
                    resolve(images
                        .map(value1 => new Image(value1.id, value1.name, theme, value1.answer, value1.question))
                    );
                })
                .catch(reject);
        });
    }

    public static getImage(id: string): Promise<Image> {
        return new Promise<Image>((resolve, reject) => {
            fetch("/api/edit/image?id=" + id)
                .then(value => value.json())
                .then(value => {
                    let image: _Image = value;
                    ThemeEditApi.getTheme(image.theme)
                        .then(value1 => resolve(new Image(image.id, image.name, value1, image.answer, image.question)))
                        .catch(reject);
                })
                .catch(reject);
        });
    }

    public static getImages(): Promise<Array<Image>> {
        return new Promise<Array<Image>>((resolve, reject) => {
            fetch("/api/edit/image")
                .then(value => value.json())
                .then(value => {
                    let images: _Image[] = value;
                    Promise.all(images.map(value1 => ThemeEditApi.getTheme(value1.theme)))
                        .then(value1 => {
                            let th: Image[] = [];
                            value1.forEach(value2 => {
                                for (let image of images) {
                                    if (image.theme == value2.id) {
                                        th.push(new Image(image.id, image.name, value2, image.answer, image.question));
                                        images.splice(images.indexOf(image), 1);
                                    }
                                }
                            });
                            resolve(th);
                        })
                        .catch(reject);
                })
                .catch(reject);
        });
    }

    public static putImage(i: Image): Promise<Image> {
        return new Promise<Image>((resolve, reject) => {
            fetch(new Request("/api/edit/image", new ImageApiRequestInit("PUT", new _Image(i.id, i.name, i.theme.id, i.answer, i.question))))
                .then(value => value.json())
                .then(value => {
                    let image: _Image = value;
                    ThemeEditApi.getTheme(image.theme)
                        .then(value1 => resolve(new Image(image.id, image.name, value1, image.answer, image.question)))
                        .catch(reject);
                })
                .catch(reject);
        });
    }

    public static deleteImage(i: Image): Promise<{}> {
        if (i.id == undefined)
            throw new Error("Image's id is undefined!");
        return fetch(new Request("/api/edit/image?id=" + i.id, new ImageApiRequestInit("DELETE")))
            .then(() => new Object());
    }

    public static updateImage(i: Image): Promise<{}> {
        if (i.id == undefined)
            throw new Error("Image's id is undefined!");
        return fetch(new Request("/api/edit/image?id=" + i.id, new ImageApiRequestInit("PATCH", new _Image(i.id, i.name, i.theme.id, i.answer, i.question))))
            .then(() => new Object());
    }

    public static getImageAddress(i: Image): string {
        return "/api/edit/image/data?id=" + i.id;
    }

    public static loadImage(i: Image, b: Blob): Promise<{}> {
        return fetch("/api/edit/image/data?id=" + i.id, new LoadImageRequestInit(b))
            .then(() => new Object());
    }
}