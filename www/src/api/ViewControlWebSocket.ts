import {Message} from "./WebSocketMessages";

export class ViewControlWebSocket {
    private socket: WebSocket;

    constructor(open: () => void, onmessage: (message: Message) => void) {
        this.socket = new WebSocket("ws://" + window.location.hostname + (window.location.port ? ":" + window.location.port : "") + "/api/control/websocket/controller");
        this.registerSocketHandlers(open, onmessage);
    }

    public send(message: Message): void {
        this.socket.send(JSON.stringify(message));
    }

    private registerSocketHandlers(open: () => void, onmessage: (message: Message) => void) {
        this.socket.onerror = ev => console.error(ev);
        this.socket.onclose = ev => {
            this.socket = new WebSocket("ws://" + window.location.hostname + (window.location.port ? ":" + window.location.port : "") + "/api/control/websocket/controller");
            this.registerSocketHandlers(open, onmessage);
        };
        this.socket.onopen = open;
        this.socket.onmessage = ev => onmessage(JSON.parse(ev.data));
    }
}