import {Message} from "./WebSocketMessages";

export class ViewWebSocket {
    private socket: WebSocket;
    private closed: boolean;

    constructor(open: () => void, close: () => void, onmessage: (message: Message) => void) {
        this.socket = new WebSocket("ws://" + window.location.hostname + (window.location.port ? ":" + window.location.port : "") + "/api/control/websocket/view");
        this.registerSocketHandlers(open, close, onmessage);
    }

    public send(message: Message): void {
        this.socket.send(JSON.stringify(message));
    }

    public close(): void {
        this.closed = true;
        this.socket.close();
    }

    private registerSocketHandlers(open: () => void, close: () => void, onmessage: (message: Message) => void) {
        this.socket.onerror = ev => console.error(ev);
        this.socket.onclose = ev => {
            if (this.closed)
                return;
            close();
            this.socket = new WebSocket("ws://" + window.location.hostname + (window.location.port ? ":" + window.location.port : "") + "/api/control/websocket/view");
            this.registerSocketHandlers(open, close, onmessage);
        };
        this.socket.onopen = open;
        this.socket.onmessage = ev => onmessage(JSON.parse(ev.data));
    }
}