export interface Message {
    id: string;
}

export class ShowImageMessage implements Message {
    image: string;
    id: string = "show-image";

    constructor(image: string) {
        this.image = image;
    }
}

export class HideImageMessage implements Message {
    image: string;
    id: string = "hide-image";

    constructor(image: string) {
        this.image = image;
    }
}

export class ClearImagesMessage implements Message {
    id: string = "clear-images";
}

export class ShowTextMessage implements Message {
    message: string;
    id: string = "show-message";
}

export class HideTextMessage implements Message {
    id: string = "hide-message";
}