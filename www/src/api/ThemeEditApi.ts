import {ThemeBlock, ThemeBlockEditApi} from "./ThemeBlockEditApi";

export class Theme {
    public id: string;
    public name: string;
    public block: ThemeBlock;

    constructor(id: string, name: string, block: ThemeBlock) {
        this.id = id;
        this.name = name;
        this.block = block;
    }
}

class _Theme {
    public id: string;
    public name: string;
    public block: string;

    constructor(id: string, name: string, block: string) {
        this.id = id;
        this.name = name;
        this.block = block;
    }
}

class ThemeApiRequestInit implements RequestInit {
    public body: string;
    public method: string;
    public headers: Headers;

    constructor(method: string, body?: object) {
        this.method = method;
        if (body != undefined) {
            this.body = JSON.stringify(body);
            this.headers = new Headers();
            this.headers.append("Content-Type", "application/json; charset=UTF-8");
        }
    }
}

export class ThemeEditApi {
    public static selectThemes(block: ThemeBlock): Promise<Array<Theme>> {
        return new Promise<Array<Theme>>((resolve, reject) => {
            fetch("/api/edit/theme/select?block_id=" + block.id)
                .then(value => value.json())
                .then(value => {
                    // noinspection UnnecessaryLocalVariableJS
                    let themes: Array<_Theme> = value;
                    let th: Array<Theme> = themes
                        .map(value1 => new Theme(value1.id, value1.name, block));
                    resolve(th);
                })
                .catch(reject);
        });
    }

    public static getTheme(id: string): Promise<Theme> {
        return new Promise<Theme>((resolve, reject) => {
            fetch("/api/edit/theme?id=" + id)
                .then(value => value.json())
                .then(value => {
                    let theme: _Theme = value;
                    ThemeBlockEditApi.getThemeBlock(theme.block)
                        .then(value1 => resolve(new Theme(theme.id, theme.name, value1)))
                        .catch(reject);
                })
                .catch(reject);
        });
    }

    public static getThemes(): Promise<Array<Theme>> {
        return new Promise<Array<Theme>>((resolve, reject) => {
            fetch("/api/edit/theme")
                .then(value => value.json())
                .then(value => {
                    let themes: _Theme[] = value;
                    Promise.all(themes.map(value1 => ThemeBlockEditApi.getThemeBlock(value1.block)))
                        .then(value1 => {
                            let th: Theme[] = [];
                            value1.forEach(value2 => {
                                for (let theme of themes) {
                                    if (theme.block == value2.id) {
                                        th.push(new Theme(theme.id, theme.name, value2));
                                        themes.splice(themes.indexOf(theme), 1);
                                    }
                                }
                            });
                            resolve(th);
                        })
                        .catch(reject);
                })
                .catch(reject);
        });
    }

    public static putTheme(th: Theme): Promise<Theme> {
        return new Promise<Theme>((resolve, reject) => {
            fetch(new Request("/api/edit/theme", new ThemeApiRequestInit("PUT", new _Theme(th.id, th.name, th.block.id))))
                .then(value => value.json())
                .then(value => {
                    let theme: _Theme = value;
                    ThemeBlockEditApi.getThemeBlock(theme.block)
                        .then(value1 => resolve(new Theme(theme.id, theme.name, value1)))
                        .catch(reject);
                })
                .catch(reject);
        });
    }

    public static deleteTheme(th: Theme): Promise<{}> {
        if (th.id == undefined)
            throw new Error("Theme's id is undefined!");
        return fetch(new Request("/api/edit/theme?id=" + th.id, new ThemeApiRequestInit("DELETE")))
            .then(() => new Object());
    }

    public static updateTheme(th: Theme): Promise<{}> {
        if (th.id == undefined)
            throw new Error("ThemeBlock's id is undefined!");
        return fetch(new Request("/api/edit/theme?id=" + th.id, new ThemeApiRequestInit("PATCH", new _Theme(th.id, th.name, th.block.id))))
            .then(() => new Object());
    }
}