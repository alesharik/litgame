export class Image {
    public id: string | null;
    public theme: string;
    public isAnswer: boolean;
    public name: string;

    constructor(theme: string, isAnswer: boolean, name: string, id?: string | null) {
        this.id = id;
        this.theme = theme;
        this.isAnswer = isAnswer;
        this.name = name;
    }
}

export class EditApi {
    public constructor() {
    }

    static getImageUrl(img: Image): string {
        return "/api/edit/image/data?id=" + img.id;
    }

    public getImages(): Promise<Array<Image>> {
        return new Promise<Array<Image>>((resolve, reject) => {
            let xlr = new XMLHttpRequest();
            xlr.onreadystatechange = ev => {
                if (xlr.readyState === 4) {
                    if (xlr.status === 200) {
                        resolve(JSON.parse(xlr.responseText));
                    } else {
                        reject(xlr.statusText);
                    }
                }
            };
            xlr.open("GET", "/api/edit/image/");
            xlr.send();
        });
    }

    public addImage(img: Image, toLoad: File): Promise<Image> {
        return new Promise<Image>((resolve, reject) => {
            let xlr = new XMLHttpRequest();
            xlr.onreadystatechange = ev => {
                if (xlr.readyState === 4) {
                    if (xlr.status === 200) {
                        let i: Image = JSON.parse(xlr.responseText);
                        this.loadImage(i, toLoad)
                            .then(() => resolve())
                            .catch(reason => reject(reason));
                    } else {
                        reject(xlr.statusText);
                    }
                }
            };
            xlr.open("PUT", "/api/edit/image/");
            debugger;
            xlr.send(JSON.stringify(img));
        });
    }

    public deleteImage(img: Image): Promise<{}> {
        return new Promise<{}>((resolve, reject) => {
            let xlr = new XMLHttpRequest();
            xlr.onreadystatechange = ev => {
                if (xlr.readyState === 4) {
                    if (xlr.status === 200) {
                        resolve();
                    } else {
                        reject(xlr.statusText);
                    }
                }
            };
            xlr.open("DELETE", "/api/edit/image/");
            xlr.send(JSON.stringify(img));
        });
    }

    public editImage(img: Image): Promise<{}> {
        return new Promise<{}>((resolve, reject) => {
            let xlr = new XMLHttpRequest();
            xlr.onreadystatechange = ev => {
                if (xlr.readyState === 4) {
                    if (xlr.status === 200) {
                        resolve();
                    } else {
                        reject(xlr.statusText);
                    }
                }
            };
            xlr.open("PATCH", "/api/edit/image/");
            xlr.send(JSON.stringify(img));
        });
    }

    public loadImage(img: Image, file: File): Promise<{}> {
        return new Promise<{}>((resolve, reject) => {
            let xlr = new XMLHttpRequest();
            xlr.onreadystatechange = ev => {
                if (xlr.readyState === 4) {
                    if (xlr.status === 200) {
                        resolve();
                    } else {
                        reject(xlr.statusText);
                    }
                }
            };
            xlr.open("PUT", "/api/edit/image/data?id=" + img.id);
            xlr.send(file.slice());
        });
    }
}