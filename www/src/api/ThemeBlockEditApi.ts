export class ThemeBlock {
    public id: string;
    public name: string;

    constructor(id: string, name: string) {
        this.id = id;
        this.name = name;
    }
}

class ThemeBlockApiRequestInit implements RequestInit {
    public body: string;
    public method: string;
    public headers: Headers;

    constructor(method: string, body?: object) {
        this.method = method;
        if (body != undefined) {
            this.body = JSON.stringify(body);
            this.headers = new Headers();
            this.headers.append("Content-Type", "application/json; charset=UTF-8");
        }
    }
}

export class ThemeBlockEditApi {
    public static getThemeBlock(id: string): Promise<ThemeBlock> {
        return fetch("/api/edit/block?id=" + id)
            .then(value => value.json());
    }

    public static getThemeBlocks(): Promise<Array<ThemeBlock>> {
        return fetch("/api/edit/block").then(value => value.json());
    }

    public static putThemeBlock(block: ThemeBlock): Promise<ThemeBlock> {
        return fetch(new Request("/api/edit/block", new ThemeBlockApiRequestInit("PUT", block)))
            .then(value => value.json());
    }

    public static deleteThemeBlock(block: ThemeBlock): Promise<{}> {
        if (block.id == undefined)
            throw new Error("ThemeBlock's id is undefined!");
        return fetch(new Request("/api/edit/block?id=" + block.id, new ThemeBlockApiRequestInit("DELETE")))
            .then(() => new Object());
    }

    public static updateThemeBlock(block: ThemeBlock): Promise<{}> {
        if (block.id == undefined)
            throw new Error("ThemeBlock's id is undefined!");
        return fetch(new Request("/api/edit/block?id=" + block.id, new ThemeBlockApiRequestInit("PATCH", block)))
            .then(() => new Object());
    }
}