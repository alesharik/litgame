export class ThemeBlock {
    public id: string;
    public name: string;

    constructor(id: string, name: string) {
        this.id = id;
        this.name = name;
    }
}

export class Theme {
    public id: string;
    public name: string;
    public block: ThemeBlock;

    constructor(id: string, name: string, block: ThemeBlock) {
        this.id = id;
        this.name = name;
        this.block = block;
    }
}

class _Theme {
    public id: string;
    public name: string;
    public block: string;

    constructor(id: string, name: string, block: string) {
        this.id = id;
        this.name = name;
        this.block = block;
    }
}

export class Image {
    public id: string;
    public name: string;
    public theme: Theme;
    public answer: boolean;
    public question: boolean;

    constructor(id: string, name: string, theme: Theme, answer: boolean, question: boolean) {
        this.id = id;
        this.name = name;
        this.theme = theme;
        this.answer = answer;
        this.question = question;
    }
}

class _Image {
    public id: string;
    public name: string;
    public theme: string;
    public answer: boolean;
    public question: boolean;

    constructor(id: string, name: string, theme: string, answer: boolean, question: boolean) {
        this.id = id;
        this.name = name;
        this.theme = theme;
        this.answer = answer;
        this.question = question;
    }
}

export class ViewControllerApi {
    static getBlocks(): Promise<Array<ThemeBlock>> {
        return fetch("/api/control/block").then(value => value.json());
    }

    static getThemes(block: ThemeBlock): Promise<Array<Theme>> {
        return new Promise<Array<Theme>>((resolve, reject) => {
            fetch("/api/control/theme?block_id=" + block.id)
                .then(value => value.json())
                .then(value => {
                    // noinspection UnnecessaryLocalVariableJS
                    let themes: Array<_Theme> = value;
                    let th: Array<Theme> = themes
                        .map(value1 => new Theme(value1.id, value1.name, block));
                    resolve(th);
                })
                .catch(reject);
        });
    }

    static getImages(theme: Theme): Promise<Array<Image>> {
        return new Promise<Array<Image>>((resolve, reject) => {
            fetch("/api/control/image?theme_id=" + theme.id)
                .then(value => value.json())
                .then(value => {
                    // noinspection UnnecessaryLocalVariableJS
                    let images: Array<_Image> = value;
                    resolve(images
                        .map(value1 => new Image(value1.id, value1.name, theme, value1.answer, value1.question))
                    );
                })
                .catch(reject);
        });
    }
}