import * as React from "react";
import {Button, Checkbox, Confirm, Icon, Image as Img, Input, Modal, Table} from "semantic-ui-react";
import {EditApi, Image} from "../../api/EditApi";

class AddModalState {
    public name: string;
    public theme: string;
    public isAnswer: boolean;
    public image: File;


    constructor(name: string, theme: string, isAnswer: boolean, image: File) {
        this.name = name;
        this.theme = theme;
        this.isAnswer = isAnswer;
        this.image = image;
    }
}

class AddModalProps {
    callback: (image: Image, file: File) => void;
}

class AddModalComponents extends React.Component<AddModalProps, AddModalState> {
    constructor(props: AddModalProps, context: any) {
        super(props, context);
        this.state = new AddModalState(undefined, undefined, false, undefined);
        this.ok = this.ok.bind(this);
        this.themeChanged = this.themeChanged.bind(this);
        this.isAnswerChanged = this.isAnswerChanged.bind(this);
        this.nameChanged = this.nameChanged.bind(this);
        this.imageChanged = this.imageChanged.bind(this);
    }

    public themeChanged(value: string) {
        this.setState(new AddModalState(this.state.name, value, this.state.isAnswer, this.state.image));
    }

    public nameChanged(value: string) {
        this.setState(new AddModalState(value, this.state.theme, this.state.isAnswer, this.state.image));
    }

    public isAnswerChanged(value: boolean) {
        this.setState(new AddModalState(this.state.name, this.state.theme, value, this.state.image));
    }

    public imageChanged(image: FileList) {
        this.setState(new AddModalState(this.state.name, this.state.theme, this.state.isAnswer, image[0]));
    }

    public ok() {
        this.props.callback(new Image(this.state.theme, this.state.isAnswer, this.state.name), this.state.image);
    }

    render() {
        let that = this;
        return (
            <Modal trigger={<Button floated={"right"} icon={true} labelPosition={"left"} primary={true}
                                    size={"small"}>
                <Icon name={"file image"}/> Add Image
            </Button>}>
                <Modal.Header>Добавление изображения</Modal.Header>
                <Modal.Content>
                    // @ts-ignore
                    <Input label={"Имя:"} onChange={event1 => that.nameChanged(event1.target.value)}/>
                    // @ts-ignore
                    <Input label={"Тема:"} onChange={event1 => that.themeChanged(event1.target.value)}/>
                    <Checkbox label={"Ответ"} toggle={true} onChange={(e, {val}) => that.isAnswerChanged(val)}/>
                    <input type={"file"} onChange={event1 => that.imageChanged(event1.target.files)}/>
                </Modal.Content>
                <Modal.Actions>
                    <Button basic color='red' inverted>
                        <Icon name='remove'/> Cancel
                    </Button>
                    <Button color='green' inverted onClick={this.ok}>
                        <Icon name='checkmark'/> Add
                    </Button>
                </Modal.Actions>
            </Modal>
        );
    }
}

class ChangeModalState {
    public name: string;
    public theme: string;
    public isAnswer: boolean;
    public image: File;
    public open: boolean;

    constructor(name: string, theme: string, isAnswer: boolean, image: File, open: boolean) {
        this.name = name;
        this.theme = theme;
        this.isAnswer = isAnswer;
        this.image = image;
        this.open = open;
    }
}

class ChangeModalProps {
    callback: (image: Image, file: File | null) => void;
    public image: Image;
}

class ChangeModalComponents extends React.Component<ChangeModalProps, ChangeModalState> {
    constructor(props: ChangeModalProps, context: any) {
        super(props, context);
        this.state = new ChangeModalState(undefined, undefined, undefined, null, false);
        this.ok = this.ok.bind(this);
        this.themeChanged = this.themeChanged.bind(this);
        this.isAnswerChanged = this.isAnswerChanged.bind(this);
        this.nameChanged = this.nameChanged.bind(this);
        this.imageChanged = this.imageChanged.bind(this);
        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
    }

    public themeChanged(value: string) {
        this.setState(new ChangeModalState(this.state.name, value, this.state.isAnswer, this.state.image, true));
    }

    public nameChanged(value: string) {
        this.setState(new ChangeModalState(value, this.state.theme, this.state.isAnswer, this.state.image, true));
    }

    public isAnswerChanged(value: boolean) {
        this.setState(new ChangeModalState(this.state.name, this.state.theme, value, this.state.image, true));
    }

    public imageChanged(image: FileList) {
        this.setState(new ChangeModalState(this.state.name, this.state.theme, this.state.isAnswer, image[0], true));
    }

    public ok() {
        console.log("test");
        this.props.callback(new Image(this.state.theme, this.state.isAnswer, this.state.name, this.props.image.id), this.state.image);
        this.close();
    }

    public close() {
        this.setState(new ChangeModalState(this.state.name, this.state.theme, this.state.isAnswer, this.state.image, false));
    }

    public open() {
        this.setState(new ChangeModalState(this.state.name, this.state.theme, this.state.isAnswer, this.state.image, true));
    }

    render() {
        let that = this;
        return (
            <Modal trigger={<Button>Редактировать</Button>} open={this.state.open} onOpen={event1 => this.open()}>
                <Modal.Header>Изменение изображения</Modal.Header>
                <Modal.Content>
                    // @ts-ignore
                    <Input label={"Имя:"}
                           onChange={event1 => that.nameChanged(event1.target.value)}>{this.props.image.name}</Input>
                    // @ts-ignore
                    <Input label={"Тема:"}
                           onChange={event1 => that.themeChanged(event1.target.value)}>{this.props.image.theme}</Input>
                    <Checkbox toggle={true} onChange={(e, {val}) => that.isAnswerChanged(val)}
                              checked={this.props.image.isAnswer}/>
                    <input type={"file"} onChange={event1 => that.imageChanged(event1.target.files)}/>
                </Modal.Content>
                <Modal.Actions>
                    <Button basic color='red' inverted onClick={() => this.close()}>
                        <Icon name='remove'/> Cancel
                    </Button>
                    <Button color='green' inverted disabled={false} onClick={() => this.ok()}
                        // disabled={this.state.theme == undefined && this.state.name == undefined && this.state.image == undefined}
                    >
                        <Icon name='checkmark'/> Update
                    </Button>
                </Modal.Actions>
            </Modal>
        );
    }
}


class ImageEditorState {
    public api: EditApi;
    public images: Array<Image>;
    public error: string | null;

    constructor(api: EditApi, images?: Array<Image>, error?: string | null) {
        this.api = api;

        if (images == undefined)
            this.images = [];
        else
            this.images = images;

        if (error == undefined)
            this.error = null;
        else
            this.error = error;
    }
}

class ImageEditor extends React.Component<{}, ImageEditorState> {
    constructor(props: {}, context: any) {
        super(props, context);
        this.state = new ImageEditorState(new EditApi());
        this.state.api.getImages().then(value => {
            this.setState(new ImageEditorState(this.state.api, value, this.state.error));
        })
            .catch(reason => this.onError(reason.toString()));
        this.editImage = this.editImage.bind(this);
        this.deleteImage = this.deleteImage.bind(this);
        this.addImage = this.addImage.bind(this);
        this.onError = this.onError.bind(this);
        this.onErrorClose = this.onErrorClose.bind(this);
    }

    public onError(err: string) {
        this.setState(new ImageEditorState(this.state.api, this.state.images, err));
    }

    public onErrorClose() {
        this.setState(new ImageEditorState(this.state.api, this.state.images, null));
    }

    public editImage(img: Image, idx: number) {
        this.state.api.editImage(img)
            .then(() => {
                this.state.images[idx] = img;
                this.setState(this.state);
            })
            .catch(reason => this.onError(reason.toString()));
    }

    public loadImage(img: Image, file: File) {
        this.state.api.loadImage(img, file)
            .then(() => {
                this.setState(this.state);
            })
            .catch(reason => this.onError(reason.toString()));
    }

    public deleteImage(img: Image) {
        this.state.api.deleteImage(img)
            .then(() => {
                this.state.images.splice(this.state.images.indexOf(img), 1);
                this.setState(this.state);
            })
            .catch(reason => this.onError(reason.toString()));
    }

    public addImage(img: Image, file: File) {
        this.state.api.addImage(img, file)
            .then(value => {
                this.state.images.push(value);
                this.setState(this.state);
            })
            .catch(reason => this.onError(reason.toString()));
    }

    public render() {
        if (this.state.error != null) {
            return (
                <Modal size={"tiny"} onClose={this.onErrorClose} open={true}>
                    <Modal.Header>
                        <p>Error</p>
                    </Modal.Header>
                    <Modal.Content>
                        <p>{this.state.error}</p>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button positive icon='checkmark' labelPosition='right' content='Ok'/>
                    </Modal.Actions>
                </Modal>
            );
        }

        let rows: Array<JSX.Element> = [];
        let that = this;
        this.state.images.forEach((value, index) => {
            rows.push((
                <Table.Row>
                    <Table.Cell>
                        <ChangeModalComponents callback={(image, file) => {
                            that.editImage(image, index);
                            if (file != null)
                                that.loadImage(image, file);
                        }} image={value}/>
                        <Confirm
                            trigger={<Button onClick={() => that.deleteImage(value)} negative={true}>Удалить</Button>}
                            onConfirm={() => that.deleteImage(value)}/>
                    </Table.Cell>
                    <Table.Cell/>
                    <Table.Cell>{value.name}</Table.Cell>
                    <Table.Cell>{value.isAnswer ? "Ответ" : "Вопрос"}</Table.Cell>
                    <Table.Cell><Img size={"small"} src={EditApi.getImageUrl(value)}/></Table.Cell>
                </Table.Row>
            ));
        });
        return (
            <Table compact={true} celled={true} definition={true} selectable={true}>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell/>
                        <Table.HeaderCell colSpan={"4"}>
                            <AddModalComponents callback={(image, file) => this.addImage(image, file)}/>
                        </Table.HeaderCell>
                    </Table.Row>
                    <Table.Row>
                        <Table.HeaderCell/>
                        <Table.HeaderCell>Имя</Table.HeaderCell>
                        <Table.HeaderCell>Тема</Table.HeaderCell>
                        <Table.HeaderCell>Вопрос/Ответ</Table.HeaderCell>
                        <Table.HeaderCell>Изображение</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                < Table.Body>
                    {rows}
                </Table.Body>
            </
                Table>
        )
            ;
    }
}

export default ImageEditor;
