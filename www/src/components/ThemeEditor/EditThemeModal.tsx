import * as React from "react";
import ErrorHandler from "../../utils/ErrorHandler";
import {Button, Dimmer, Icon, Input, Loader, Modal} from "semantic-ui-react";
import {Theme, ThemeEditApi} from "../../api/ThemeEditApi";

export class EditThemeModalProps {
    public theme: Theme;
    public open: boolean;
    public onComplete: (block: Theme) => void;
    public onReject: () => void;
}

class EditThemeModalState {
    public theme: Theme;
    public inProgress: boolean;

    constructor(theme: Theme, inProgress: boolean) {
        this.theme = theme;
        this.inProgress = inProgress;
    }
}

export class EditThemeModal extends React.Component<EditThemeModalProps, EditThemeModalState> {
    constructor(props: EditThemeModalProps, context: any) {
        super(props, context);
        this.state = new EditThemeModalState(props.theme, false);
        this.onComplete = this.onComplete.bind(this);
        this.nameChanged = this.nameChanged.bind(this);
    }

    onComplete() {
        this.setState(new EditThemeModalState(this.state.theme, true));
        ThemeEditApi.updateTheme(this.state.theme)
            .then(() => {
                this.setState(new EditThemeModalState(this.state.theme, false));
                this.props.onComplete(this.state.theme);
            })
            .catch(reason => {
                ErrorHandler.handleHttpError(reason);
                this.setState(new EditThemeModalState(this.state.theme, false));
            });
    }

    nameChanged(value: string) {
        this.setState(new EditThemeModalState(new Theme(this.state.theme.id, value, this.state.theme.block), false));
    }

    render() {
        return (
            <Modal open={this.props.open} closeOnDimmerClick={!this.state.inProgress}>
                <Dimmer active={this.state.inProgress}>
                    <Loader/>
                </Dimmer>
                <Modal.Header>Изменение темы</Modal.Header>
                <Modal.Content>
                    <Input label={"Имя: "} placeholder={this.props.theme.name} onChange={event1 => {
                        // @ts-ignore
                        this.nameChanged(event1.target.value);
                    }} error={this.state.theme.name == undefined || this.state.theme.name == ""}/>
                </Modal.Content>
                <Modal.Actions>
                    <Button color={"red"} onClick={this.props.onReject}>
                        <Icon name={"cancel"}/> Отмена
                    </Button>
                    <Button color={"green"} onClick={this.onComplete}
                            disabled={this.state.theme.name == undefined || this.state.theme.name == ""}>
                        <Icon name={"check"}/> Изменить
                    </Button>
                </Modal.Actions>
            </Modal>
        );
    }
}