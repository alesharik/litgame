import * as React from "react";
import {Button, Icon} from "semantic-ui-react";
import "./TreeButton.less";
import {Image} from "../../api/ImageEditApi";
import {EditImageModal} from "./EditImageModal";

export class EditImageButtonProps {
    image: Image;
    onEditCompleted: (image: Image) => void;
}

class EditImageButtonState {
    modalOpen: boolean;

    constructor(modalOpen: boolean) {
        this.modalOpen = modalOpen;
    }
}

export class EditImageButton extends React.Component<EditImageButtonProps, EditImageButtonState> {
    constructor(props: EditImageButtonProps, context: any) {
        super(props, context);
        this.state = new EditImageButtonState(false);

        this.openModal = this.openModal.bind(this);
        this.handleReject = this.handleReject.bind(this);
        this.handleComplete = this.handleComplete.bind(this);
    }

    openModal() {
        this.setState(new EditImageButtonState(true));
    }

    handleReject() {
        this.setState(new EditImageButtonState(false));
    }

    handleComplete(image: Image) {
        this.setState(new EditImageButtonState(false));
        this.props.onEditCompleted(image);
    }

    render() {
        return (
            <div>
                <Button primary={true} onClick={this.openModal} size={"tiny"} className={"tree-button"} basic={true}>
                    <Icon name={"edit"}/>Изменить
                </Button>
                <EditImageModal image={this.props.image} open={this.state.modalOpen} onComplete={this.handleComplete}
                                onReject={this.handleReject}/>
            </div>
        );
    }
}