import * as React from "react";
import {Button, Icon} from "semantic-ui-react";
import "./TreeButton.less";
import {Image} from "../../api/ImageEditApi";
import {DeleteImageModal} from "./DeleteImageModal";

export class DeleteImageButtonProps {
    image: Image;
    onDeleteCompleted: (image: Image) => void;
}

class DeleteImageButtonState {
    modalOpen: boolean;

    constructor(modalOpen: boolean) {
        this.modalOpen = modalOpen;
    }
}

export class DeleteImageButton extends React.Component<DeleteImageButtonProps, DeleteImageButtonState> {
    constructor(props: DeleteImageButtonProps, context: any) {
        super(props, context);
        this.state = new DeleteImageButtonState(false);

        this.openModal = this.openModal.bind(this);
        this.handleReject = this.handleReject.bind(this);
        this.handleComplete = this.handleComplete.bind(this);
    }

    openModal() {
        this.setState(new DeleteImageButtonState(true));
    }

    handleReject() {
        this.setState(new DeleteImageButtonState(false));
    }

    handleComplete(image: Image) {
        this.setState(new DeleteImageButtonState(false));
        this.props.onDeleteCompleted(image);
    }

    render() {
        return (
            <div>
                <Button color={"red"} onClick={this.openModal} size={"tiny"} className={"tree-button"} basic={true}>
                    <Icon name={"delete"}/>Удалить
                </Button>
                <DeleteImageModal image={this.props.image} open={this.state.modalOpen} onComplete={this.handleComplete}
                                  onReject={this.handleReject}/>
            </div>
        );
    }
}