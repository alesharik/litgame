import * as React from "react";
import {Button, Icon} from "semantic-ui-react";
import {Image} from "../../api/ImageEditApi";
import "./TreeButton.less";
import {Theme} from "../../api/ThemeEditApi";
import {AddImageModal} from "./AddImageModal";

export class AddImageButtonProps {
    theme: Theme;
    onAdded: (image: Image) => void;
}

class AddImageButtonState {
    modalOpen: boolean;

    constructor(modalOpen: boolean) {
        this.modalOpen = modalOpen;
    }
}

export class AddImageButton extends React.Component<AddImageButtonProps, AddImageButtonState> {
    constructor(props: AddImageButtonProps, context: any) {
        super(props, context);
        this.state = new AddImageButtonState(false);

        this.openModal = this.openModal.bind(this);
        this.handleReject = this.handleReject.bind(this);
        this.handleComplete = this.handleComplete.bind(this);
    }

    openModal() {
        this.setState(new AddImageButtonState(true));
    }

    handleReject() {
        this.setState(new AddImageButtonState(false));
    }

    handleComplete(image: Image) {
        this.setState(new AddImageButtonState(false));
        this.props.onAdded(image);
    }

    render() {
        return (
            <div>
                <Button color={"green"} onClick={this.openModal} size={"tiny"} className={"tree-button spaced"}
                        basic={true}>
                    <Icon name={"plus"}/>Добавить изображение
                </Button>
                <AddImageModal theme={this.props.theme} open={this.state.modalOpen} onComplete={this.handleComplete}
                               onReject={this.handleReject}/>
            </div>
        );
    }
}