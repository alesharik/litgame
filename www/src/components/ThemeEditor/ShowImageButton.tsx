import * as React from "react";
import {Image, ImageEditApi} from "../../api/ImageEditApi";
import {Button, Image as Img, Modal} from "semantic-ui-react";
import "./TreeButton.less";

export class ShowImageButtonProps {
    image: Image;

    constructor(image: Image) {
        this.image = image;
    }
}

class ShowImageButtonState {
    open: boolean;

    constructor(open: boolean) {
        this.open = open;
    }
}

export class ShowImageButton extends React.Component<ShowImageButtonProps, ShowImageButtonState> {
    constructor(props: ShowImageButtonProps, context: any) {
        super(props, context);
        this.state = new ShowImageButtonState(false);
    }

    render() {
        return <div>
            <Button onClick={() => this.setState(new ShowImageButtonState(true))} color={"green"}
                    className={"tree-button"} basic={true} size={"tiny"}>
                Показать изображение
            </Button>
            <Modal open={this.state.open}>
                <Modal.Header>
                    <p>Просмотр изображения</p>
                </Modal.Header>
                <Modal.Content>
                    <Img src={ImageEditApi.getImageAddress(this.props.image)}/>
                </Modal.Content>
                <Modal.Actions>
                    <Button color={"red"}
                            onClick={() => this.setState(new ShowImageButtonState(false))}>Закрыть</Button>
                </Modal.Actions>
            </Modal>
        </div>;
    }
}