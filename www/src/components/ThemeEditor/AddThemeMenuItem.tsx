import * as React from "react";
import {ThemeBlock} from "../../api/ThemeBlockEditApi";
import {Button, Menu} from "semantic-ui-react";
import {Theme} from "../../api/ThemeEditApi";
import {AddThemeModal} from "./AddThemeModal";

export class AddThemeModalProps {
    public onAdd: (theme: Theme) => void;
    public block: ThemeBlock;

    constructor(onAdd: (theme: Theme) => void) {
        this.onAdd = onAdd;
    }
}

class AddThemeModalState {
    public modalOpen: boolean;

    constructor(modalOpen: boolean) {
        this.modalOpen = modalOpen;
    }
}

export class AddThemeBlockItem extends React.Component<AddThemeModalProps, AddThemeModalState> {
    constructor(props: AddThemeModalProps, context: any) {
        super(props, context);
        this.state = new AddThemeModalState(false);

        this.buttonClicked = this.buttonClicked.bind(this);
        this.onAdd = this.onAdd.bind(this);
        this.onReject = this.onReject.bind(this);
    }

    buttonClicked() {
        this.setState(new AddThemeModalState(true));
    }

    onAdd(theme: Theme) {
        this.setState(new AddThemeModalState(false));
        this.props.onAdd(theme);
    }

    onReject() {
        this.setState(new AddThemeModalState(false));
    }

    render() {
        return (
            <Menu.Item position={"right"}>
                <Button onClick={this.buttonClicked} primary={true}>
                    Добавить тему
                </Button>
                <AddThemeModal onComplete={this.onAdd} open={this.state.modalOpen} onReject={this.onReject}
                               block={this.props.block}/>
            </Menu.Item>
        );
    }
}