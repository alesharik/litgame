import * as React from "react";
import {Button, Icon} from "semantic-ui-react";
import "./TreeButton.less";
import {Theme} from "../../api/ThemeEditApi";
import {DeleteThemeModal} from "./DeleteThemeModal";

export class DeleteThemeButtonProps {
    theme: Theme;
    onDeleteCompleted: (theme: Theme) => void;
}

class DeleteThemeButtonState {
    modalOpen: boolean;

    constructor(modalOpen: boolean) {
        this.modalOpen = modalOpen;
    }
}

export class DeleteThemeButton extends React.Component<DeleteThemeButtonProps, DeleteThemeButtonState> {
    constructor(props: DeleteThemeButtonProps, context: any) {
        super(props, context);
        this.state = new DeleteThemeButtonState(false);

        this.openModal = this.openModal.bind(this);
        this.handleReject = this.handleReject.bind(this);
        this.handleComplete = this.handleComplete.bind(this);
    }

    openModal() {
        this.setState(new DeleteThemeButtonState(true));
    }

    handleReject() {
        this.setState(new DeleteThemeButtonState(false));
    }

    handleComplete(theme: Theme) {
        this.setState(new DeleteThemeButtonState(false));
        this.props.onDeleteCompleted(theme);
    }

    render() {
        return (
            <div>
                <Button color={"red"} onClick={this.openModal} size={"tiny"} className={"tree-button"} basic={true}>
                    <Icon name={"delete"}/>Удалить
                </Button>
                <DeleteThemeModal theme={this.props.theme} open={this.state.modalOpen} onComplete={this.handleComplete}
                                  onReject={this.handleReject}/>
            </div>
        );
    }
}