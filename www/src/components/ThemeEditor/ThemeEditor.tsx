import * as React from "react";
import SortableTree, {
    addNodeUnderParent,
    changeNodeAtPath,
    getNodeAtPath,
    removeNodeAtPath,
    TreeItem,
    TreeNode
} from "react-sortable-tree";
import {ThemeBlock, ThemeBlockEditApi} from "../../api/ThemeBlockEditApi";
import {Theme, ThemeEditApi} from "../../api/ThemeEditApi";
import ErrorHandler from "../../utils/ErrorHandler";
import {Image, ImageEditApi} from "../../api/ImageEditApi";
import {Button, Container, Dimmer, Icon, Input, Loader, Menu} from "semantic-ui-react";
import {RouteComponentProps} from "react-router";
import "./ThemeEditor.less";
import {Link} from "react-router-dom";
import {AddThemeBlockItem} from "./AddThemeMenuItem";
import {EditThemeButton} from "./EditThemeButton";
import {DeleteThemeButton} from "./DeleteThemeButton";
import {AddImageButton} from "./AddImageButton";
import {DeleteImageButton} from "./DeleteImageButton";
import {EditImageButton} from "./EditImageButton";
import {ShowImageButton} from "./ShowImageButton";

interface MatchParams {
    block: string;
}

class ThemeTreeItem implements TreeItem {
    children: TreeItem[];
    expanded: boolean;
    title: React.ReactNode;
    theme: Theme;
    public id: string;
    public type: number;

    constructor(expanded: boolean, theme: Theme, children: Array<TreeItem>, id?: string) {
        this.children = children;
        this.expanded = expanded;
        this.title = <p>{theme.name}</p>;
        this.theme = theme;
        this.id = id == undefined ? Math.random().toString(36).substring(2) : id;
        this.type = 1;
    }
}

class ImageTreeItem implements TreeItem {
    expanded: boolean;
    title: React.ReactNode;
    image: Image;
    public id: string;
    public type: number;

    constructor(image: Image, id?: string) {
        this.expanded = false;
        this.title =
            <p>{image.name + "(" + (image.question ? "Вопрос " : "") + (image.answer ? "Ответ " : "") + ")"}</p>;
        this.image = image;
        this.id = id == undefined ? Math.random().toString(36).substring(2) : id;
        this.type = 2;
    }
}

class ThemeEditorState {
    public treeItems: Array<TreeItem>;
    public block: ThemeBlock;
    public loading: boolean;
    public search: string;

    constructor(treeItems: Array<TreeItem>, loading?: boolean, block?: ThemeBlock, search?: string) {
        this.loading = loading == undefined;
        this.treeItems = treeItems;
        this.block = block;
        this.search = search == undefined ? "" : search;
    }
}

export class ThemeEditor extends React.Component<RouteComponentProps<MatchParams>, ThemeEditorState> {
    constructor(props: RouteComponentProps<MatchParams>, context: any) {
        super(props, context);
        this.state = new ThemeEditorState([]);
        this.addTheme = this.addTheme.bind(this);
        this.addImage = this.addImage.bind(this);
        this.deleteTheme = this.deleteTheme.bind(this);
        this.addImage = this.addImage.bind(this);
        this.changeTheme = this.changeTheme.bind(this);
        this.addImage = this.addImage.bind(this);

        let b = this.props.match.params.block;
        ThemeBlockEditApi.getThemeBlock(b)
            .then(block => {
                this.setState(new ThemeEditorState([], true, block, this.state.search));
                let items: Array<TreeItem> = [];
                ThemeEditApi.selectThemes(block)
                    .then(value => {
                        Promise.all(value
                            .map(value1 => ImageEditApi.selectImages(value1))
                        )
                            .then(imagePromises => {
                                value.forEach(theme => {
                                    let imgs: Array<TreeItem> = [];
                                    imagePromises.forEach(img => {
                                        img.forEach((i) => {
                                            if (i.theme.id == theme.id)
                                                imgs.push(new ImageTreeItem(i));
                                        });
                                    });
                                    items.push(new ThemeTreeItem(false, theme, imgs));
                                });
                            })
                            .then(() => this.setState(new ThemeEditorState(items, false, block, this.state.search)))
                            .catch(reason => ErrorHandler.handleHttpError(reason));
                    })
                    .catch(reason => ErrorHandler.handleHttpError(reason));

            })
            .catch(reason => ErrorHandler.handleHttpError(reason));
    }

    static nodeId(node: TreeNode) {
        return node.node.id;
    }

    addImage(i: Image, parentPath: any) {
        let s = this.state;
        let t = addNodeUnderParent({
            getNodeKey: ThemeEditor.nodeId,
            treeData: s.treeItems,
            parentKey: parentPath,
            newNode: new ImageTreeItem(i),
            ignoreCollapsed: false,
            expandParent: true
        }).treeData;
        this.setState(new ThemeEditorState(
            t,
            s.loading,
            s.block,
            s.search
        ));
    }

    changeImage(i: Image, path: any) {
        let s = this.state;
        let node: ImageTreeItem = getNodeAtPath({
            treeData: s.treeItems,
            path,
            getNodeKey: ThemeEditor.nodeId
        }).node as ImageTreeItem;
        this.setState(new ThemeEditorState(
            changeNodeAtPath({
                treeData: s.treeItems,
                path,
                getNodeKey: ThemeEditor.nodeId,
                newNode: new ImageTreeItem(i, node.id)
            }),
            s.loading,
            s.block,
            s.search
        ));
    }

    deleteImage(i: Image, path: any) {
        let s = this.state;
        this.setState(new ThemeEditorState(
            removeNodeAtPath({
                treeData: s.treeItems,
                path,
                getNodeKey: ThemeEditor.nodeId
            }),
            s.loading,
            s.block,
            s.search
        ));
    }

    addTheme(t: Theme) {
        let s = this.state;
        s.treeItems.push(new ThemeTreeItem(false, t, []));
        this.setState(new ThemeEditorState(
            s.treeItems,
            s.loading,
            s.block,
            s.search
        ));
    }

    changeTheme(t: Theme, path: any) {
        let s = this.state;
        let node: ThemeTreeItem = getNodeAtPath({
            treeData: s.treeItems,
            path,
            getNodeKey: ThemeEditor.nodeId
        }).node as ThemeTreeItem;
        this.setState(new ThemeEditorState(
            changeNodeAtPath({
                treeData: s.treeItems,
                path,
                getNodeKey: ThemeEditor.nodeId,
                newNode: new ThemeTreeItem(node.expanded, t, node.children, node.id)
            }),
            s.loading,
            s.block,
            s.search
        ));
    }

    deleteTheme(t: Theme, path: any) {
        let s = this.state;
        this.setState(new ThemeEditorState(
            removeNodeAtPath({
                treeData: s.treeItems,
                path,
                getNodeKey: ThemeEditor.nodeId
            }),
            s.loading,
            s.block,
            s.search
        ));
    }

    render() {
        return (
            <div className={"editor"}>
                <Menu fixed={"top"} inverted={true}>
                    <Menu.Item>
                        <Link to={"/edit/block"}>
                            <Button inverted={true}>
                                <Icon name={"arrow left"}/>
                            </Button>
                        </Link>
                    </Menu.Item>
                    <Menu.Item header={true}>{this.state.block == undefined ? "" : this.state.block.name}</Menu.Item>
                    <AddThemeBlockItem onAdd={this.addTheme} block={this.state.block}/>
                    <Menu.Item>
                        <Input placeholder={"Поиск"} onChange={(e) => {
                            // @ts-ignore
                            let s = e.target.value;
                            this.setState(new ThemeEditorState(this.state.treeItems, this.state.loading, this.state.block, s));
                        }} disabled={true}/>
                    </Menu.Item>
                </Menu>
                <Container className={"editor-container"}>
                    <Dimmer active={this.state.loading}>
                        <Loader/>
                    </Dimmer>
                    <SortableTree treeData={this.state.treeItems} generateNodeProps={data => {
                        if (data.node.type == 1) {
                            return {
                                buttons: [
                                    <EditThemeButton theme={data.node.theme}
                                                     onEditCompleted={theme => this.changeTheme(theme, data.path)}/>,
                                    <DeleteThemeButton theme={data.node.theme}
                                                       onDeleteCompleted={theme => this.deleteTheme(theme, data.path)}/>,
                                    <AddImageButton theme={data.node.theme}
                                                    onAdded={image => this.addImage(image, data.path)}/>
                                ]
                            };
                        } else if (data.node.type == 2) {
                            return {
                                buttons: [
                                    <ShowImageButton image={data.node.image}/>,
                                    <EditImageButton image={data.node.image}
                                                     onEditCompleted={image => this.changeImage(image, data.path)}/>,
                                    <DeleteImageButton image={data.node.image}
                                                       onDeleteCompleted={image => this.deleteImage(image, data.path)}/>
                                ]
                            };
                        }
                        return {};
                    }}
                                  onChange={treeData => this.setState(new ThemeEditorState(treeData, false, this.state.block, this.state.search))}
                                  searchQuery={this.state.search}
                                  getNodeKey={data => data.node.id}
                                  canDrag={data => data.node.type != 1}
                                  canDrop={data => data.nextParent != undefined && data.nextParent.type == 1 && data.node.type == 2}
                                  onMoveNode={data => {
                                      let i: Image = data.node.image;
                                      let newTheme: Theme = data.nextParentNode.theme;
                                      ImageEditApi.updateImage(new Image(i.id, undefined, newTheme, undefined, undefined))
                                          .catch(reason => {
                                              ErrorHandler.handleHttpError(reason);
                                          });
                                  }}
                        // searchMethod={data =>
                        // @ts-ignore
                        // data.searchQuery && data.node.title.textContent.toString().toLowerCase().indexOf(data.searchQuery.toLowerCase()) > -1
                        //}
                    />
                </Container>
            </div>
        );
    }
}