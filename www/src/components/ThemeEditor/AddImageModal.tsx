import {Theme} from "../../api/ThemeEditApi";
import * as React from "react";
import ErrorHandler from "../../utils/ErrorHandler";
import {Button, Checkbox, Dimmer, Form, Icon, Input, Label, Loader, Modal} from "semantic-ui-react";
import {Image, ImageEditApi} from "../../api/ImageEditApi";

export class AddImageModalProps {
    public onComplete: (image: Image) => void;
    public onReject: () => void;
    public open: boolean;
    public theme: Theme;
}

class AddImageModalState {
    public inProgress: boolean;
    public image: Image;
    public data: Blob;

    constructor(image: Image, data: Blob, inProgress: boolean) {
        this.inProgress = inProgress;
        this.data = data;
        this.image = image;
    }
}

export class AddImageModal extends React.Component<AddImageModalProps, AddImageModalState> {
    constructor(props: AddImageModalProps, context: any) {
        super(props, context);
        this.state = new AddImageModalState(new Image(undefined, undefined, this.props.theme, false, false), undefined, false);

        this.onComplete = this.onComplete.bind(this);
        this.nameChanged = this.nameChanged.bind(this);
        this.answerChanged = this.answerChanged.bind(this);
        this.questionChanged = this.questionChanged.bind(this);
        this.dataChanged = this.dataChanged.bind(this);
    }

    onComplete() {
        this.setState(new AddImageModalState(this.state.image, this.state.data, true));
        ImageEditApi.putImage(this.state.image)
            .then(value => {
                ImageEditApi.loadImage(value, this.state.data).then(() => {
                    this.setState(new AddImageModalState(this.state.image, this.state.data, false));
                    this.props.onComplete(value);
                }).catch(reason => {
                    ErrorHandler.handleHttpError(reason);
                    this.setState(new AddImageModalState(this.state.image, this.state.data, false));
                });
            })
            .catch(reason => {
                ErrorHandler.handleHttpError(reason);
                this.setState(new AddImageModalState(this.state.image, this.state.data, false));
            });
    }

    nameChanged(value: string) {
        this.setState(new AddImageModalState(new Image(undefined, value, this.props.theme, this.state.image.answer, this.state.image.question), this.state.data, false));
    }

    answerChanged(value: boolean) {
        this.setState(new AddImageModalState(new Image(undefined, this.state.image.name, this.props.theme, value, this.state.image.question), this.state.data, false));
    }

    questionChanged(value: boolean) {
        this.setState(new AddImageModalState(new Image(undefined, this.state.image.name, this.props.theme, this.state.image.answer, value), this.state.data, false));
    }

    dataChanged(data: Blob) {
        this.setState(new AddImageModalState(new Image(undefined, this.state.image.name, this.props.theme, this.state.image.answer, this.state.image.question), data, false));
    }

    render() {
        let id = Math.random().toString(36).substring(2);
        return (
            <Modal open={this.props.open} closeOnDimmerClick={!this.state.inProgress}>
                <Dimmer active={this.state.inProgress}>
                    <Loader/>
                </Dimmer>
                <Modal.Header>Добавление изображения</Modal.Header>
                <Modal.Content>
                    <Form>
                        <Form.Field>
                            <Input label={"Имя: "} onChange={event1 => {
                                // @ts-ignore
                                this.nameChanged(event1.target.value);
                            }} error={this.state.image.name == undefined || this.state.image.name == ""}/>
                        </Form.Field>
                        <Form.Field>
                            <Checkbox label={"Является вопросом: "}
                                      onChange={(event1, data) => this.questionChanged(data.checked)} toggle={true}/>
                        </Form.Field>
                        <Form.Field>
                            <Checkbox label={"Является ответом: "}
                                      onChange={(event1, data) => this.answerChanged(data.checked)} toggle={true}/>
                        </Form.Field>
                        <Form.Field>
                            <Label
                                as="label"
                                basic={true}
                                htmlFor={id}
                            >
                                <Button
                                    icon="upload"
                                    label={{
                                        basic: true,
                                        content: "Select file(s)"
                                    }}
                                    labelPosition="right"
                                />
                                <input
                                    id={id}
                                    hidden={true}
                                    onChange={event1 => {
                                        let file = event1.target.files[0];
                                        this.dataChanged(file.slice(0));
                                    }}
                                    type="file"
                                />
                            </Label>
                        </Form.Field>
                    </Form>
                </Modal.Content>
                <Modal.Actions>
                    <Button color={"red"} onClick={this.props.onReject}>
                        <Icon name={"cancel"}/> Отмена
                    </Button>
                    <Button color={"green"} onClick={this.onComplete}
                            disabled={
                                this.state.image.name == undefined
                                || this.state.image.name == ""
                                || this.state.image.question == undefined
                                || this.state.image.answer == undefined
                                || this.state.data == undefined
                            }>
                        <Icon name={"check"}/> Добавить
                    </Button>
                </Modal.Actions>
            </Modal>
        );
    }
}