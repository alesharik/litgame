import * as React from "react";
import {Confirm} from "semantic-ui-react";
import ErrorHandler from "../../utils/ErrorHandler";
import {Theme, ThemeEditApi} from "../../api/ThemeEditApi";

export class DeleteThemeModalProps {
    public theme: Theme;
    public open: boolean;
    public onComplete: (block: Theme) => void;
    public onReject: () => void;
}

class DeleteThemeModalState {
    public inProgress: boolean;

    constructor(inProgress: boolean) {
        this.inProgress = inProgress;
    }
}

export class DeleteThemeModal extends React.Component<DeleteThemeModalProps, DeleteThemeModalState> {
    constructor(props: DeleteThemeModalProps, context: any) {
        super(props, context);
        this.state = new DeleteThemeModalState(false);
        this.onConfirm = this.onConfirm.bind(this);
    }

    onConfirm() {
        this.setState(new DeleteThemeModalState(true));
        ThemeEditApi.deleteTheme(this.props.theme)
            .then(() => {
                this.setState(new DeleteThemeModalState(false));
                this.props.onComplete(this.props.theme);
            })
            .catch(reason => {
                ErrorHandler.handleHttpError(reason);
                this.setState(new DeleteThemeModalState(false));
            });
    }

    render() {
        return (
            <Confirm
                open={this.props.open}
                header={"Удаление темы"}
                content={"Вы уверены, что хотите удалить эту тему?"}
                cancelButton={"Отмена"}
                confirmButton={"Удалить"}
                onConfirm={this.onConfirm}
                onCancel={this.props.onReject}/>
        );
    }
}