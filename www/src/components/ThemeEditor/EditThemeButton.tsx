import * as React from "react";
import {Theme} from "../../api/ThemeEditApi";
import {Button, Icon} from "semantic-ui-react";
import {EditThemeModal} from "./EditThemeModal";
import "./TreeButton.less";

export class EditThemeButtonProps {
    theme: Theme;
    onEditCompleted: (theme: Theme) => void;
}

class EditThemeButtonState {
    modalOpen: boolean;

    constructor(modalOpen: boolean) {
        this.modalOpen = modalOpen;
    }
}

export class EditThemeButton extends React.Component<EditThemeButtonProps, EditThemeButtonState> {
    constructor(props: EditThemeButtonProps, context: any) {
        super(props, context);
        this.state = new EditThemeButtonState(false);

        this.openModal = this.openModal.bind(this);
        this.handleReject = this.handleReject.bind(this);
        this.handleComplete = this.handleComplete.bind(this);
    }

    openModal() {
        this.setState(new EditThemeButtonState(true));
    }

    handleReject() {
        this.setState(new EditThemeButtonState(false));
    }

    handleComplete(theme: Theme) {
        this.setState(new EditThemeButtonState(false));
        this.props.onEditCompleted(theme);
    }

    render() {
        return (
            <div>
                <Button primary={true} onClick={this.openModal} size={"tiny"} className={"tree-button"} basic={true}>
                    <Icon name={"edit"}/>Изменить
                </Button>
                <EditThemeModal theme={this.props.theme} open={this.state.modalOpen} onComplete={this.handleComplete}
                                onReject={this.handleReject}/>
            </div>
        );
    }
}