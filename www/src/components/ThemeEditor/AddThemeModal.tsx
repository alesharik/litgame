import * as React from "react";
import {Button, Dimmer, Icon, Input, Loader, Modal} from "semantic-ui-react";
import ErrorHandler from "../../utils/ErrorHandler";
import {Theme, ThemeEditApi} from "../../api/ThemeEditApi";
import {ThemeBlock} from "../../api/ThemeBlockEditApi";

export class AddThemeModalProps {
    public onComplete: (theme: Theme) => void;
    public onReject: () => void;
    public open: boolean;
    public block: ThemeBlock;
}

class AddThemeModalState {
    public inProgress: boolean;
    public theme: Theme;

    constructor(theme: Theme, inProgress: boolean) {
        this.inProgress = inProgress;
        this.theme = theme;
    }
}

export class AddThemeModal extends React.Component<AddThemeModalProps, AddThemeModalState> {
    constructor(props: AddThemeModalProps, context: any) {
        super(props, context);
        this.state = new AddThemeModalState(new Theme(undefined, undefined, this.props.block), false);

        this.onComplete = this.onComplete.bind(this);
        this.nameChanged = this.nameChanged.bind(this);
    }

    onComplete() {
        this.setState(new AddThemeModalState(this.state.theme, true));
        ThemeEditApi.putTheme(this.state.theme)
            .then(value => {
                this.setState(new AddThemeModalState(this.state.theme, false));
                this.props.onComplete(value);
            })
            .catch(reason => {
                ErrorHandler.handleHttpError(reason);
                this.setState(new AddThemeModalState(this.state.theme, false));
            });
    }

    nameChanged(value: string) {
        this.setState(new AddThemeModalState(new Theme(undefined, value, this.props.block), false));
    }

    render() {
        return (
            <Modal open={this.props.open} closeOnDimmerClick={!this.state.inProgress}>
                <Dimmer active={this.state.inProgress}>
                    <Loader/>
                </Dimmer>
                <Modal.Header>Добавление темы</Modal.Header>
                <Modal.Content>
                    <Input label={"Имя: "} onChange={event1 => {
                        // @ts-ignore
                        this.nameChanged(event1.target.value);
                    }} error={this.state.theme.name == undefined || this.state.theme.name == ""}/>
                </Modal.Content>
                <Modal.Actions>
                    <Button color={"red"} onClick={this.props.onReject}>
                        <Icon name={"cancel"}/> Отмена
                    </Button>
                    <Button color={"green"} onClick={this.onComplete}
                            disabled={this.state.theme.name == undefined || this.state.theme.name == ""}>
                        <Icon name={"check"}/> Добавить
                    </Button>
                </Modal.Actions>
            </Modal>
        );
    }
}