import * as React from "react";
import {Confirm} from "semantic-ui-react";
import ErrorHandler from "../../utils/ErrorHandler";
import {Image, ImageEditApi} from "../../api/ImageEditApi";

export class DeleteImageModalProps {
    public image: Image;
    public open: boolean;
    public onComplete: (image: Image) => void;
    public onReject: () => void;
}

class DeleteImageModalState {
    public inProgress: boolean;

    constructor(inProgress: boolean) {
        this.inProgress = inProgress;
    }
}

export class DeleteImageModal extends React.Component<DeleteImageModalProps, DeleteImageModalState> {
    constructor(props: DeleteImageModalProps, context: any) {
        super(props, context);
        this.state = new DeleteImageModalState(false);
        this.onConfirm = this.onConfirm.bind(this);
    }

    onConfirm() {
        this.setState(new DeleteImageModalState(true));
        ImageEditApi.deleteImage(this.props.image)
            .then(() => {
                this.setState(new DeleteImageModalState(false));
                this.props.onComplete(this.props.image);
            })
            .catch(reason => {
                ErrorHandler.handleHttpError(reason);
                this.setState(new DeleteImageModalState(false));
            });
    }

    render() {
        return (
            <Confirm
                open={this.props.open}
                header={"Удаление изображения"}
                content={"Вы уверены, что хотите удалить это изображение?"}
                cancelButton={"Отмена"}
                confirmButton={"Удалить"}
                onConfirm={this.onConfirm}
                onCancel={this.props.onReject}/>
        );
    }
}