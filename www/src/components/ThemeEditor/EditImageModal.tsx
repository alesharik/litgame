import * as React from "react";
import ErrorHandler from "../../utils/ErrorHandler";
import {Button, Checkbox, Dimmer, Form, Icon, Input, Label, Loader, Modal} from "semantic-ui-react";
import {Image, ImageEditApi} from "../../api/ImageEditApi";
import "./EditImageModal.less";

export class EditImageModalProps {
    public image: Image;
    public open: boolean;
    public onComplete: (image: Image) => void;
    public onReject: () => void;
}

class EditImageModalState {
    public image: Image;
    public data: Blob;
    public inProgress: boolean;

    constructor(image: Image, data: Blob, inProgress: boolean) {
        this.image = image;
        this.data = data;
        this.inProgress = inProgress;
    }
}

export class EditImageModal extends React.Component<EditImageModalProps, EditImageModalState> {
    constructor(props: EditImageModalProps, context: any) {
        super(props, context);
        this.state = new EditImageModalState(props.image, undefined, false);
        this.onComplete = this.onComplete.bind(this);
        this.nameChanged = this.nameChanged.bind(this);
    }

    onComplete() {
        this.setState(new EditImageModalState(this.state.image, this.state.data, true));//FIXME
        ImageEditApi.updateImage(this.state.image)
            .then(() => {
                if (this.state.data != undefined) {
                    ImageEditApi.loadImage(this.state.image, this.state.data)
                        .then(() => {
                            this.setState(new EditImageModalState(this.state.image, this.state.data, false));
                            this.props.onComplete(this.state.image);
                        })
                        .catch(reason => {
                            ErrorHandler.handleHttpError(reason);
                            this.setState(new EditImageModalState(this.state.image, this.state.data, false));
                        });
                } else {
                    this.setState(new EditImageModalState(this.state.image, this.state.data, false));
                    this.props.onComplete(this.state.image);
                }
            })
            .catch(reason => {
                ErrorHandler.handleHttpError(reason);
                this.setState(new EditImageModalState(this.state.image, this.state.data, false));
            });

    }

    nameChanged(value: string) {
        this.setState(new EditImageModalState(new Image(this.state.image.id, value, this.state.image.theme, this.state.image.answer, this.state.image.question), this.state.data, false));
    }

    answerChanged(answer: boolean) {
        this.setState(new EditImageModalState(new Image(this.state.image.id, this.state.image.name, this.state.image.theme, answer, this.state.image.question), this.state.data, false));
    }

    questionChanged(question: boolean) {
        this.setState(new EditImageModalState(new Image(this.state.image.id, this.state.image.name, this.state.image.theme, this.state.image.answer, question), this.state.data, false));
    }

    dataChanged(data: Blob) {
        this.setState(new EditImageModalState(new Image(this.state.image.id, this.state.image.name, this.state.image.theme, this.state.image.answer, this.state.image.question), data, false));
    }

    render() {
        let id = Math.random().toString(36).substring(2);
        return (
            <Modal open={this.props.open} closeOnDimmerClick={!this.state.inProgress}>
                <Dimmer active={this.state.inProgress}>
                    <Loader/>
                </Dimmer>
                <Modal.Header>Изменение изображения</Modal.Header>
                <Modal.Content>
                    <Form>
                        <Form.Field>
                            <Input label={"Имя: "} onChange={event1 => {
                                // @ts-ignore
                                this.nameChanged(event1.target.value);
                            }} error={this.state.image.name == undefined || this.state.image.name == ""}
                                   placeholder={this.props.image.name}
                                   value={this.state.image.name == undefined ? this.props.image.name : this.state.image.name}/>
                        </Form.Field>
                        <Form.Field>
                            <Checkbox label={"Является вопросом: "}
                                      onClick={(event1, data) => this.questionChanged(data.checked)}
                                      toggle={true}
                                      checked={this.state.image.question == undefined ? this.props.image.question : this.state.image.question}/>
                        </Form.Field>
                        <Form.Field>
                            <Checkbox label={"Является ответом: "}
                                      onClick={(event1, data) => this.answerChanged(data.checked)}
                                      toggle={true}
                                      checked={this.state.image.answer == undefined ? this.props.image.answer : this.state.image.answer}/>
                        </Form.Field>
                        <Form.Field>
                            <Label
                                as="label"
                                basic={true}
                                htmlFor={id}
                            >
                                <Button
                                    icon="upload"
                                    label={{
                                        basic: true,
                                        content: "Select file(s)"
                                    }}
                                    labelPosition="right"
                                />
                                <input
                                    id={id}
                                    hidden={true}
                                    onChange={event1 => {
                                        let file = event1.target.files[0];
                                        this.dataChanged(file.slice(0));
                                    }}
                                    type="file"
                                />
                            </Label>
                        </Form.Field>
                    </Form>
                </Modal.Content>
                <Modal.Actions>
                    <Button color={"red"} onClick={this.props.onReject}>
                        <Icon name={"cancel"}/> Отмена
                    </Button>
                    <Button color={"green"} onClick={this.onComplete}
                            disabled={(this.state.image.name == undefined || this.state.image.name == "") && this.state.image.question == undefined && this.state.image.answer == undefined && this.state.data == undefined}>
                        <Icon name={"check"}/> Изменить
                    </Button>
                </Modal.Actions>
            </Modal>
        );
    }
}