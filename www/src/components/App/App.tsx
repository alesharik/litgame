import "./App.less";
import * as React from "react";
import {BrowserRouter, Route, RouteProps, Switch} from "react-router-dom";
import TitleScreen from "../TitleScreen/TitleScreen";
import {ThemeBlockEditor} from "../ThemeBlockEditor/ThemeBlockEditor";
import {ThemeEditor} from "../ThemeEditor/ThemeEditor";
import {View} from "../View/View";
import {ViewController} from "../ViewController/ViewController";

class App extends React.Component<RouteProps> {
    public render() {
        return (<div style={{height: "100vh"}}>
                {/*<ErrorHandlerModal/>*/}
                <BrowserRouter>
                    <Switch>
                        <Route exact={true} path="/" component={TitleScreen}/>
                        <Route path="/edit/block/" exact={true} component={ThemeBlockEditor}/>
                        <Route path="/edit/block/:block" component={ThemeEditor}/>
                        <Route path="/control/view" component={View}/>
                        <Route path="/control/controller" component={ViewController}/>
                    </Switch>
                </BrowserRouter>
            </div>
        );
    }
}

export default App;
