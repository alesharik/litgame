import * as React from "react";
import {ViewWebSocket} from "../../api/ViewWebSocket";
import {ViewApi} from "../../api/ViewApi";
import {HideImageMessage, ShowImageMessage, ShowTextMessage} from "../../api/WebSocketMessages";
import {Dimmer, Image, Loader, Modal} from "semantic-ui-react";
import {ImageGrid} from "../ImageGrid/ImageGrid";
import "./View.less";

class ViewState {
    connected: boolean;
    images: string[];
    socket: ViewWebSocket;
    message: string;

    constructor(connected: boolean, images: string[], socket: ViewWebSocket, message?: string) {
        this.connected = connected;
        this.images = images;
        this.socket = socket;
        this.message = message;
    }
}

export class View extends React.Component<{}, ViewState> {
    constructor(props: {}, context: any) {
        super(props, context);
        let socket = new ViewWebSocket(() => {
                this.setState(new ViewState(true, this.state.images, this.state.socket));
            },
            () => {
                this.setState(new ViewState(false, this.state.images, this.state.socket));
            },
            message => {
                if (message.id == "show-image") {
                    let msg = message as ShowImageMessage;
                    let i = this.state.images;
                    i.push(ViewApi.getImageUrl(msg.image));
                    this.setState(new ViewState(this.state.connected, i, this.state.socket, this.state.message));
                } else if (message.id == "hide-image") {
                    let msg = message as HideImageMessage;
                    let i = this.state.images;
                    let idx = -1;
                    i.forEach((value, index) => {
                        if (value.endsWith(msg.image))
                            idx = index;
                    });
                    i.splice(idx, 1);
                    this.setState(new ViewState(this.state.connected, i, this.state.socket, this.state.message));
                } else if (message.id == "show-message") {
                    let m = message as ShowTextMessage;
                    this.setState(new ViewState(this.state.connected, this.state.images, this.state.socket, m.message));
                } else if (message.id == "hide-message") {
                    this.setState(new ViewState(this.state.connected, this.state.images, this.state.socket, undefined));
                } else if (message.id == "clear-images") {
                    this.setState(new ViewState(this.state.connected, [], this.state.socket, this.state.message));
                }
            });
        this.state = new ViewState(false, [], socket);
    }

    componentWillUnmount(): void {
        this.state.socket.close();
        super.componentWillUnmount();
    }

    render() {
        return (
            <div>
                <Dimmer active={!this.state.connected}>
                    <Loader/>
                </Dimmer>
                <Modal open={this.state.message != undefined} basic={true}>
                    <Modal.Content>
                        <h1>{this.state.message}</h1>
                    </Modal.Content>
                </Modal>
                <ImageGrid images={this.state.images} factory={(url, key, style) => (
                    <Image src={url} key={key} style={style} className={"view-image"}/>
                )}/>
            </div>
        );
    }
}