import * as React from "react";
import {CSSProperties} from "react";
import {Image} from "semantic-ui-react";
import "./ImageGrid.less";

export class ImageGridProps {
    images: Array<string>;

    constructor(images: Array<string>, factory?: (url: string) => React.ReactNode) {
        this.images = images;
        this.factory = factory == undefined ? (url, key) => <Image src={url} key={key}/> : factory;
    }

    factory?: (url: string, key: string, style: CSSProperties) => React.ReactNode = (url, key, style) => <Image
        src={url} key={key} style={style}/>;
}

export class ImageGrid extends React.Component<ImageGridProps> {
    constructor(props: ImageGridProps, context: any) {
        super(props, context);
    }

    render() {
        let rows = Math.round(Math.sqrt(this.props.images.length));
        let items: React.ReactNode[] = [];
        let off = 0;
        for (let i = 0; i < rows; i++) {
            let a: React.ReactNode[] = [];
            for (let j = 0; j < Math.ceil(this.props.images.length / rows); j++) {
                if (this.props.images[off + j] == undefined)
                    break;

                let maxWidth = "calc(" + 100 / Math.ceil(this.props.images.length / rows) + "vw - " + Math.ceil(this.props.images.length / rows) + "em)";
                let maxHeight = "calc(" + 100 / rows + "vw - " + rows + "em)";
                a.push(this.props.factory(this.props.images[off + j], this.props.images[off + j], {
                    maxWidth,
                    maxHeight
                }));
            }
            off += Math.ceil(this.props.images.length / rows);
            items.push(<div className={"row"} key={i + 10000}>{a}</div>);
        }
        return (
            <div style={{overflow: "hidden"}} className={"image-grid"}>
                {items}
            </div>
        );
    }
}