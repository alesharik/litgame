import * as React from "react";

export class SwitchableImageProps {
    onStateChanged: (state: boolean) => void;
    defaultState?: boolean = false;
    imageFactory: (checked: boolean, clicked: () => void) => React.ReactNode;

    constructor(onStateChanged: (state: boolean) => void, imageFactory: (checked: boolean, clicked: () => void) => React.ReactNode, defaultState?: boolean) {
        this.onStateChanged = onStateChanged;
        this.defaultState = defaultState;
        this.imageFactory = imageFactory;
    }
}

class SwitchableImageState {
    switched: boolean;

    constructor(switched: boolean) {
        this.switched = switched;
    }
}

export class SwitchableImage extends React.Component<SwitchableImageProps, SwitchableImageState> {
    constructor(props: SwitchableImageProps, context: any) {
        super(props, context);
        this.state = new SwitchableImageState(this.props.defaultState);
        this.clicked = this.clicked.bind(this);
    }

    clicked() {
        this.props.onStateChanged(!this.state.switched);
        this.setState(new SwitchableImageState(!this.state.switched));
    }

    render() {
        return (
            <div>
                {this.props.imageFactory(this.state.switched, this.clicked)}
            </div>
        );
    }
}