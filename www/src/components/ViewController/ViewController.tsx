import * as React from "react";
import {ViewControlWebSocket} from "../../api/ViewControlWebSocket";
import {Image, Theme, ThemeBlock, ViewControllerApi} from "../../api/ViewControllerApi";
import {Button, Dimmer, Grid, Image as Img, Label, Loader, Menu, Segment} from "semantic-ui-react";
import SortableTree, {changeNodeAtPath, getNodeAtPath, TreeItem} from "react-sortable-tree";
import ErrorHandler from "../../utils/ErrorHandler";
import {SwitchableImage} from "./SwitchableImage";
import "./ViewController.less";
import {ClearImagesMessage, HideImageMessage, ShowImageMessage} from "../../api/WebSocketMessages";
import {ViewApi} from "../../api/ViewApi";

class ViewControllerState {
    socket: ViewControlWebSocket;
    loading: boolean;
    items: Array<TreeItem>;
    imagesLoading: boolean;
    images: Array<Image>;
    shownImages: Array<string>;
    lastSelectedImagePath: any;

    constructor(socket: ViewControlWebSocket, loading: boolean, items: Array<TreeItem>, imagesLoading: boolean, images: Array<Image>, shownImages: Array<string>, lastSelectedImagePath: any) {
        this.socket = socket;
        this.loading = loading;
        this.items = items;
        this.imagesLoading = imagesLoading;
        this.images = images;
        this.shownImages = shownImages;
        this.lastSelectedImagePath = lastSelectedImagePath;
    }
}

class BlockTreeItem implements TreeItem {
    children: TreeItem[];
    expanded: boolean;
    title: React.ReactNode;
    selected: boolean;
    id: string = "block";
    block: ThemeBlock;

    constructor(children: TreeItem[], expanded: boolean, title: string, themeBlock: ThemeBlock) {
        this.children = children;
        this.expanded = expanded;
        this.title = <p>{title}</p>;
        this.selected = expanded;
        this.block = themeBlock;
    }
}

class ThemeTreeItem implements TreeItem {
    children: TreeItem[] = [];
    expanded: boolean = false;
    title: React.ReactNode;
    selected: boolean;
    id: string = "theme";
    theme: Theme;
    visited: boolean;

    constructor(title: string, selected: boolean, theme: Theme, visited?: boolean) {
        this.title = <p>{title}</p>;
        this.selected = selected;
        this.theme = theme;
        this.visited = visited != undefined && visited;
    }
}

export class ViewController extends React.Component<{}, ViewControllerState> {
    constructor(props: {}, context: any) {
        super(props, context);
        this.state = new ViewControllerState(new ViewControlWebSocket(() => {
            ViewControllerApi.getBlocks().then(value => {
                Promise.all(value
                    .map(value1 => ViewControllerApi.getThemes(value1))
                )
                    .then(value1 => {
                        let items: Array<TreeItem> = [];
                        value1.forEach(value2 => {
                            if (value2.length == 0)
                                return;

                            let block = value2[0].block;
                            let blockItem = new BlockTreeItem(value2.sort((a, b) => a.name.toString().localeCompare(b.name.toString())).map((e: Theme) => new ThemeTreeItem(e.name, false, e)), true, block.name, block);
                            items.push(blockItem);
                        });
                        this.setState(new ViewControllerState(this.state.socket, false, items, this.state.imagesLoading, this.state.images, this.state.shownImages, this.state.lastSelectedImagePath));
                    })
                    .catch(reason => ErrorHandler.handleHttpError(reason));
            });
        }, message => console.log(message)), true, [], false, [], [], undefined);

        this.onImageStateChanged = this.onImageStateChanged.bind(this);
        this.onThemeSelected = this.onThemeSelected.bind(this);
    }

    onImageStateChanged(image: Image, checked: boolean) {
        if (checked) {
            this.state.shownImages.push(image.id);
            this.state.socket.send(new ShowImageMessage(image.id));
        } else {
            this.state.shownImages.splice(this.state.shownImages.indexOf(image.id), 1);
            this.state.socket.send(new HideImageMessage(image.id));
        }
        this.setState(new ViewControllerState(this.state.socket, false, this.state.items, this.state.imagesLoading, this.state.images, this.state.shownImages, this.state.lastSelectedImagePath));

    }

    onThemeSelected(path: any, theme: Theme) {
        this.state.socket.send(new ClearImagesMessage());
        if (this.state.lastSelectedImagePath != undefined) {
            let node = getNodeAtPath({
                path: this.state.lastSelectedImagePath,
                treeData: this.state.items,
                getNodeKey: data => data.node.id == "theme" ? data.node.theme.id : data.node.block.id
            }).node;
            this.setState(new ViewControllerState(this.state.socket, false, changeNodeAtPath({
                path: this.state.lastSelectedImagePath,
                getNodeKey: data => data.node.id == "theme" ? data.node.theme.id : data.node.block.id,
                treeData: this.state.items,
                newNode: new ThemeTreeItem(node.theme.name, false, node.theme, true)
            }), true, this.state.images, this.state.shownImages, this.state.lastSelectedImagePath));
        }
        this.setState(new ViewControllerState(this.state.socket, false, changeNodeAtPath({
            path,
            getNodeKey: data => data.node.id == "theme" ? data.node.theme.id : data.node.block.id,
            treeData: this.state.items,
            newNode: new ThemeTreeItem(theme.name, true, theme)
        }), true, this.state.images, this.state.shownImages, path));
        ViewControllerApi.getImages(theme)
            .then(value => {
                this.setState(new ViewControllerState(this.state.socket, false, this.state.items, false, value, [], this.state.lastSelectedImagePath));
            })
            .catch(reason => ErrorHandler.handleHttpError(reason));
    }

    render() {
        return (
            <div style={{overflow: "hidden", height: "100vh"}}>
                <Dimmer active={this.state.loading}>
                    <Loader/>
                </Dimmer>
                <Menu inverted={true} fixed={"top"}>
                    <Menu.Item header={true}>Контроллер</Menu.Item>
                    <Menu.Item position={"right"}>
                        <Button color={"red"} onClick={() => this.state.socket.send(new ClearImagesMessage())}>
                            Очистить экран
                        </Button>
                    </Menu.Item>
                    <Menu.Item>
                        <Button disabled={true}>
                            Сообщение
                        </Button>
                    </Menu.Item>
                </Menu>
                <Grid style={{marginTop: "0.5em", height: "100vh"}}>
                    <Grid.Column width={4}>
                        <SortableTree treeData={this.state.items}
                                      onChange={treeData => new ViewControllerState(this.state.socket, this.state.loading, treeData, this.state.imagesLoading, this.state.images, this.state.shownImages, this.state.lastSelectedImagePath)}
                                      canDrop={() => true}
                                      canDrag={() => false}
                                      generateNodeProps={data => {
                                          if (data.node.id == "theme") {
                                              let className = "not-visited-tree-item";
                                              if (data.node.selected)
                                                  className = "selected-tree-item";
                                              else if (data.node.visited)
                                                  className = "visited-tree-item";
                                              return {
                                                  className,
                                                  onClick: () => this.onThemeSelected(data.path, data.node.theme)
                                              };
                                          }
                                      }}
                                      getNodeKey={data => data.node.id == "theme" ? data.node.theme.id : (data.node.id == "block" ? data.node.block.id : "")}/>
                    </Grid.Column>
                    <Grid.Column width={8} style={{overflowY: "auto"}}>
                        <Dimmer active={this.state.imagesLoading}>
                            <Loader/>
                        </Dimmer>
                        <Segment>
                            <h3>Вопрос</h3>
                            <div className={"image-selector"}>
                                {
                                    this.state.images
                                        .sort((a, b) => a.name.toString().localeCompare(b.name.toString()))
                                        .filter(value => value.question)
                                        .map(value => (
                                            <SwitchableImage
                                                onStateChanged={state => this.onImageStateChanged(value, state)}
                                                imageFactory={(checked, i) => {
                                                    return <Img label={<Label>{value.name}</Label>} key={value.id}
                                                                src={ViewApi.getImageUrl(value.id)} onClick={i}
                                                                className={checked ? "switching-image" : "switching-image shadowed"}
                                                                size={"small"}/>;
                                                }} key={value.id}/>
                                        ))
                                }
                            </div>
                        </Segment>
                        <Segment>
                            <h3>Ответ</h3>
                            <div className={"image-selector"}>
                                {
                                    this.state.images
                                        .sort((a, b) => a.name.toString().localeCompare(b.name.toString()))
                                        .filter(value => value.answer)
                                        .map(value => (
                                            <SwitchableImage
                                                onStateChanged={state => this.onImageStateChanged(value, state)}
                                                imageFactory={(checked, i) => {
                                                    return <Img label={<Label>{value.name}</Label>} key={value.id}
                                                                src={ViewApi.getImageUrl(value.id)} onClick={i}
                                                                className={checked ? "switching-image" : "switching-image shadowed"}
                                                                size={"small"}/>;
                                                }} key={value.id}/>
                                        ))
                                }
                            </div>
                        </Segment>
                    </Grid.Column>
                    <Grid.Column width={4}>
                        {/*<View/>*/}
                    </Grid.Column>
                </Grid>
            </div>
        );
    }
}