import * as React from "react";
import "./ThemeBlockEditor.less";
import {ThemeBlock, ThemeBlockEditApi} from "../../api/ThemeBlockEditApi";
import {ThemeBlockItem} from "./ThemeBlockItem";
import {AddThemeBlockItem} from "./AddThemeBlockItem";
import ErrorHandler from "../../utils/ErrorHandler";
import {Dimmer, Loader} from "semantic-ui-react";

class ThemeBlockEditorState {
    public blocks: Array<ThemeBlock>;

    constructor(blocks?: Array<ThemeBlock>) {
        this.blocks = blocks;
    }
}

export class ThemeBlockEditor extends React.Component<{}, ThemeBlockEditorState> {
    constructor(props: {}, context: any) {
        super(props, context);
        this.state = new ThemeBlockEditorState();
        ThemeBlockEditApi.getThemeBlocks()
            .then(value => this.setState(new ThemeBlockEditorState(value)))
            .catch(reason => ErrorHandler.handleHttpError(reason));

        this.addBlock = this.addBlock.bind(this);
        this.editBlock = this.editBlock.bind(this);
        this.removeBlock = this.removeBlock.bind(this);
    }

    addBlock(block: ThemeBlock) {
        this.state.blocks.push(block);
        this.setState(new ThemeBlockEditorState(this.state.blocks));
    }

    editBlock(block: ThemeBlock) {
    }

    removeBlock(block: ThemeBlock) {
        let n: Array<ThemeBlock> = [];
        this.state.blocks
            .filter(value => value.id != block.id)
            .forEach(value => n.push(value));
        this.setState(new ThemeBlockEditorState(n));
    }

    render() {
        let items: Array<JSX.Element> = [];
        if (this.state.blocks != undefined) {
            this.state.blocks.forEach((value) => items.push(<ThemeBlockItem block={value}
                                                                            onChange={this.editBlock}
                                                                            onDelete={this.removeBlock}
                                                                            key={value.id}/>));
            items.push(<AddThemeBlockItem onAdd={this.addBlock} key={-1}/>);
        }
        return (
            <div>
                <Dimmer active={this.state.blocks == undefined}>
                    <Loader/>
                </Dimmer>
                <div className={"theme-block-editor"}>
                    {items}
                </div>
            </div>
        );
    }
}