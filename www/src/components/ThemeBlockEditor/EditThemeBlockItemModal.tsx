import * as React from "react";
import {ThemeBlock, ThemeBlockEditApi} from "../../api/ThemeBlockEditApi";
import ErrorHandler from "../../utils/ErrorHandler";
import {Button, Dimmer, Icon, Input, Loader, Modal} from "semantic-ui-react";

export class EditThemeBlockItemModalProps {
    public block: ThemeBlock;
    public open: boolean;
    public onComplete: (block: ThemeBlock) => void;
    public onReject: () => void;
}

class EditThemeBlockItemModalState {
    public block: ThemeBlock;
    public inProgress: boolean;

    constructor(block: ThemeBlock, inProgress: boolean) {
        this.block = block;
        this.inProgress = inProgress;
    }
}

export class EditThemeBlockItemModal extends React.Component<EditThemeBlockItemModalProps, EditThemeBlockItemModalState> {
    constructor(props: EditThemeBlockItemModalProps, context: any) {
        super(props, context);
        this.state = new EditThemeBlockItemModalState(props.block, false);
        this.onComplete = this.onComplete.bind(this);
        this.nameChanged = this.nameChanged.bind(this);
    }

    onComplete() {
        this.setState(new EditThemeBlockItemModalState(this.state.block, true));
        ThemeBlockEditApi.updateThemeBlock(this.state.block)
            .then(() => {
                this.setState(new EditThemeBlockItemModalState(this.state.block, false));
                this.props.onComplete(this.state.block);
            })
            .catch(reason => {
                ErrorHandler.handleHttpError(reason);
                this.setState(new EditThemeBlockItemModalState(this.state.block, false));
            });
    }

    nameChanged(value: string) {
        this.setState(new EditThemeBlockItemModalState(new ThemeBlock(this.state.block.id, value), false));
    }

    render() {
        return (
            <Modal open={this.props.open} closeOnDimmerClick={!this.state.inProgress}>
                <Dimmer active={this.state.inProgress}>
                    <Loader/>
                </Dimmer>
                <Modal.Header>Изменение блока</Modal.Header>
                <Modal.Content>
                    <Input label={"Имя: "} placeholder={this.props.block.name} onChange={event1 => {
                        // @ts-ignore
                        this.nameChanged(event1.target.value);
                    }} error={this.state.block.name == undefined || this.state.block.name == ""}/>
                </Modal.Content>
                <Modal.Actions>
                    <Button color={"red"} onClick={this.props.onReject}>
                        <Icon name={"cancel"}/> Отмена
                    </Button>
                    <Button color={"green"} onClick={this.onComplete}
                            disabled={this.state.block.name == undefined || this.state.block.name == ""}>
                        <Icon name={"check"}/> Изменить
                    </Button>
                </Modal.Actions>
            </Modal>
        );
    }
}