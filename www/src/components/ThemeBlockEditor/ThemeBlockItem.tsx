import * as React from "react";
import {ThemeBlock} from "../../api/ThemeBlockEditApi";
import {Button, Icon} from "semantic-ui-react";
import {Link} from "react-router-dom";
import "./ThemeBlockItem.less";
import {EditThemeBlockItemModal} from "./EditThemeBlockItemModal";
import {DeleteThemeBlockItemModal} from "./DeleteThemeBlockItemModal";

export class ThemeBlockItemProps {
    public block: ThemeBlock;
    public onDelete: (block: ThemeBlock) => void;
    public onChange: (block: ThemeBlock) => void;

    constructor(block: ThemeBlock) {
        this.block = block;
    }
}

class ThemeBlockItemState {
    public block: ThemeBlock;
    public editModalOpen: boolean;
    public deleteModalOpen: boolean;

    constructor(block: ThemeBlock, editModalOpen: boolean, deleteModalOpen: boolean) {
        this.block = block;
        this.editModalOpen = editModalOpen;
        this.deleteModalOpen = deleteModalOpen;
    }
}

export class ThemeBlockItem extends React.Component<ThemeBlockItemProps, ThemeBlockItemState> {
    constructor(props: ThemeBlockItemProps, context: any) {
        super(props, context);
        this.state = new ThemeBlockItemState(props.block, false, false);

        this.onChange = this.onChange.bind(this);
        this.onDeleteRejected = this.onDeleteRejected.bind(this);
        this.onEditRejected = this.onEditRejected.bind(this);
        this.onEditButtonClicked = this.onEditButtonClicked.bind(this);
        this.onDeleteButtonClicked = this.onDeleteButtonClicked.bind(this);
        this.onDelete = this.onDelete.bind(this);
    }

    onChange(block: ThemeBlock) {
        this.setState(new ThemeBlockItemState(block, false, false));
        this.props.onChange(block);
    }

    onEditRejected() {
        this.setState(new ThemeBlockItemState(this.state.block, false, false));
    }

    onDeleteRejected() {
        this.setState(new ThemeBlockItemState(this.state.block, false, false));
    }

    onDelete() {
        this.setState(new ThemeBlockItemState(this.state.block, false, false));
        this.props.onDelete(this.state.block);
    }

    onEditButtonClicked() {
        this.setState(new ThemeBlockItemState(this.state.block, true, false));
    }

    onDeleteButtonClicked() {
        this.setState(new ThemeBlockItemState(this.state.block, false, true));
    }

    render() {
        return (
            <span className={"theme-block-item"}>
                <Link to={"/edit/block/" + this.state.block.id} className={"open-section"}>
                    <Button>
                        {this.state.block.name}
                    </Button>
                </Link>
                <div>
                    <Button className={"edit-section"} onClick={this.onEditButtonClicked} color={"yellow"}><Icon
                        name={"edit outline"}/></Button>
                    <Button className={"delete-section"} onClick={this.onDeleteButtonClicked} color={"red"}><Icon
                        name={"delete"}/></Button>
                    <EditThemeBlockItemModal block={this.state.block} open={this.state.editModalOpen}
                                             onComplete={this.onChange} onReject={this.onEditRejected}/>
                    <DeleteThemeBlockItemModal block={this.state.block} open={this.state.deleteModalOpen}
                                               onComplete={this.onDelete} onReject={this.onDeleteRejected}/>
                </div>
            </span>
        );
    }
}