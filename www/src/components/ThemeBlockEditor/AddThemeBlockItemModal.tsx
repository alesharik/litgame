import * as React from "react";
import {ThemeBlock, ThemeBlockEditApi} from "../../api/ThemeBlockEditApi";
import {Button, Dimmer, Icon, Input, Loader, Modal} from "semantic-ui-react";
import ErrorHandler from "../../utils/ErrorHandler";

export class AddThemeBlockItemModalProps {
    public onComplete: (block: ThemeBlock) => void;
    public onReject: () => void;
    public open: boolean;
}

class AddThemeBlockItemModalState {
    public inProgress: boolean;
    public block: ThemeBlock;

    constructor(block: ThemeBlock, inProgress: boolean) {
        this.inProgress = inProgress;
        this.block = block;
    }
}

export class AddThemeBlockItemModal extends React.Component<AddThemeBlockItemModalProps, AddThemeBlockItemModalState> {
    constructor(props: AddThemeBlockItemModalProps, context: any) {
        super(props, context);
        this.state = new AddThemeBlockItemModalState(new ThemeBlock(undefined, undefined), false);

        this.onComplete = this.onComplete.bind(this);
        this.nameChanged = this.nameChanged.bind(this);
    }

    onComplete() {
        this.setState(new AddThemeBlockItemModalState(this.state.block, true));
        ThemeBlockEditApi.putThemeBlock(this.state.block)
            .then(value => {
                this.setState(new AddThemeBlockItemModalState(this.state.block, false));
                this.props.onComplete(value);
            })
            .catch(reason => {
                ErrorHandler.handleHttpError(reason);
                this.setState(new AddThemeBlockItemModalState(this.state.block, false));
            });
    }

    nameChanged(value: string) {
        this.setState(new AddThemeBlockItemModalState(new ThemeBlock(undefined, value), false));
    }

    render() {
        return (
            <Modal open={this.props.open} closeOnDimmerClick={!this.state.inProgress}>
                <Dimmer active={this.state.inProgress}>
                    <Loader/>
                </Dimmer>
                <Modal.Header>Добавление блока</Modal.Header>
                <Modal.Content>
                    <Input label={"Имя: "} onChange={event1 => {
                        // @ts-ignore
                        this.nameChanged(event1.target.value);
                    }} error={this.state.block.name == undefined || this.state.block.name == ""}/>
                </Modal.Content>
                <Modal.Actions>
                    <Button color={"red"} onClick={this.props.onReject}>
                        <Icon name={"cancel"}/> Отмена
                    </Button>
                    <Button color={"green"} onClick={this.onComplete}
                            disabled={this.state.block.name == undefined || this.state.block.name == ""}>
                        <Icon name={"check"}/> Добавить
                    </Button>
                </Modal.Actions>
            </Modal>
        );
    }
}