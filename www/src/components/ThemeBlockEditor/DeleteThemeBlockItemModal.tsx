import * as React from "react";
import {ThemeBlock, ThemeBlockEditApi} from "../../api/ThemeBlockEditApi";
import {Confirm} from "semantic-ui-react";
import ErrorHandler from "../../utils/ErrorHandler";

export class DeleteThemeBlockItemModalProps {
    public block: ThemeBlock;
    public open: boolean;
    public onComplete: (block: ThemeBlock) => void;
    public onReject: () => void;
}

class DeleteThemeBlockItemModalState {
    public inProgress: boolean;

    constructor(inProgress: boolean) {
        this.inProgress = inProgress;
    }
}

export class DeleteThemeBlockItemModal extends React.Component<DeleteThemeBlockItemModalProps, DeleteThemeBlockItemModalState> {
    constructor(props: DeleteThemeBlockItemModalProps, context: any) {
        super(props, context);
        this.state = new DeleteThemeBlockItemModalState(false);
        this.onConfirm = this.onConfirm.bind(this);
    }

    onConfirm() {
        this.setState(new DeleteThemeBlockItemModalState(true));
        ThemeBlockEditApi.deleteThemeBlock(this.props.block)
            .then(() => {
                this.setState(new DeleteThemeBlockItemModalState(false));
                this.props.onComplete(this.props.block);
            })
            .catch(reason => {
                ErrorHandler.handleHttpError(reason);
                this.setState(new DeleteThemeBlockItemModalState(false));
            });
    }

    render() {
        return (
            <Confirm
                open={this.props.open}
                header={"Удаление элемента"}
                content={"Вы уверены, что хотите удалить этот блок?"}
                cancelButton={"Отмена"}
                confirmButton={"Удалить"}
                onConfirm={this.onConfirm}
                onCancel={this.props.onReject}/>
        );
    }
}