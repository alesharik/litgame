import * as React from "react";
import {ThemeBlock} from "../../api/ThemeBlockEditApi";
import "./ThemeBlockItem.less";
import {AddThemeBlockItemModal} from "./AddThemeBlockItemModal";
import {Button, Icon} from "semantic-ui-react";

export class AddThemeBlockItemProps {
    public onAdd: (block: ThemeBlock) => void;

    constructor(onAdd: (block: ThemeBlock) => void) {
        this.onAdd = onAdd;
    }
}

class AddThemeBlockItemState {
    public modalOpen: boolean;

    constructor(modalOpen: boolean) {
        this.modalOpen = modalOpen;
    }
}

export class AddThemeBlockItem extends React.Component<AddThemeBlockItemProps, AddThemeBlockItemState> {
    constructor(props: AddThemeBlockItemProps, context: any) {
        super(props, context);
        this.state = new AddThemeBlockItemState(false);

        this.buttonClicked = this.buttonClicked.bind(this);
        this.onAdd = this.onAdd.bind(this);
        this.onReject = this.onReject.bind(this);
    }

    buttonClicked() {
        this.setState(new AddThemeBlockItemState(true));
    }

    onAdd(block: ThemeBlock) {
        this.setState(new AddThemeBlockItemState(false));
        this.props.onAdd(block);
    }

    onReject() {
        this.setState(new AddThemeBlockItemState(false));
    }

    render() {
        return (
            <div className={"theme-block-item"}>
                <Button onClick={this.buttonClicked} basic={true}>
                    <Icon name={"add"}/>
                </Button>
                <AddThemeBlockItemModal onComplete={this.onAdd} open={this.state.modalOpen} onReject={this.onReject}/>
            </div>
        );
    }
}