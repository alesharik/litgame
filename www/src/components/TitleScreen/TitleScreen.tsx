import * as React from "react";
import {Link} from "react-router-dom";
import {Button, Grid} from "semantic-ui-react";

class TitleScreen extends React.Component {
    public render() {
        return (
            <Grid centered={true} columns={1}>
                <Grid.Column>
                    <Button as={Link} to={"/edit/block/"}>Edit images</Button>
                    <Button as={Link} to={"/control/view/"}>Control view</Button>
                    <Button as={Link} to={"/control/controller"}>Controller</Button>
                </Grid.Column>
            </Grid>
        );
    }
}

export default TitleScreen;
