import "./index.less";
import "semantic-ui-less/semantic.less";
import "react-sortable-tree/style.css";
import * as React from "react";
import * as ReactDOM from "react-dom";
import App from "./components/App/App";
import registerServiceWorker from "./registerServiceWorker";

ReactDOM.render(
    <App/>,
    document.getElementById("root"),
);
registerServiceWorker();
