package com.alesharik.litgame;

import com.alesharik.database.Database;
import com.alesharik.database.connection.FixedThreadBoundConnectionPool;
import com.alesharik.database.data.Schema;
import com.alesharik.database.driver.postgres.PostgresDriver;
import com.alesharik.litgame.data.DataManager;
import com.alesharik.litgame.http.HttpBundle;
import com.alesharik.webserver.extension.module.Configuration;
import com.alesharik.webserver.extension.module.ConfigurationValue;
import com.alesharik.webserver.extension.module.LinkModule;
import com.alesharik.webserver.extension.module.Module;
import com.alesharik.webserver.extension.module.Shutdown;
import com.alesharik.webserver.extension.module.ShutdownNow;
import com.alesharik.webserver.extension.module.Start;
import com.alesharik.webserver.module.http.server.HttpServer;

@Module("litgame")
public final class LitGameModule {
    @Configuration
    private LitGameConfiguration configuration;

    private Database database;

    @Start
    private void start() {
        FixedThreadBoundConnectionPool connection = new FixedThreadBoundConnectionPool(
                configuration.database.databaseAddress,
                configuration.database.userName,
                configuration.database.password,
                10
        );
        Database database = Database.newDatabase(
                connection,
                new PostgresDriver(),
                true
        );
        database.init();
        this.database = database;

        Schema schema = database.getSchema(configuration.database.schemaName, true);
        DataManager dataManager = new DataManager(schema, database.getTransactionManager());

        configuration.httpServer.addHttpHandlerBundle(new HttpBundle(dataManager));
    }

    @Shutdown
    @ShutdownNow
    private void shutdown() {
        database.close();
    }

    @Configuration
    private static final class LitGameConfiguration {
        @ConfigurationValue("database")
        private DatabaseConfiguration database;
        @LinkModule("http-server")
        private HttpServer httpServer;

        private static final class DatabaseConfiguration {
            @ConfigurationValue("address")
            private String databaseAddress;
            @ConfigurationValue("schema")
            private String schemaName;
            @ConfigurationValue("user")
            private String userName;
            @ConfigurationValue("password")
            private String password;
        }
    }
}
