package com.alesharik.litgame;

import com.alesharik.webserver.configuration.config.lang.ApiEndpointSection;
import com.alesharik.webserver.configuration.config.lang.ConfigurationEndpoint;
import com.alesharik.webserver.configuration.config.lang.ConfigurationModule;
import com.alesharik.webserver.configuration.config.lang.CustomEndpointSection;
import com.alesharik.webserver.configuration.config.lang.ExternalLanguageHelper;
import com.alesharik.webserver.configuration.config.lang.ScriptEndpointSection;
import com.alesharik.webserver.configuration.config.lang.element.ConfigurationElement;
import com.alesharik.webserver.configuration.config.lang.element.ConfigurationPrimitive;
import com.alesharik.webserver.configuration.config.lang.element.ConfigurationTypedObject;
import com.alesharik.webserver.configuration.config.lang.parser.ConfigurationParser;
import com.alesharik.webserver.configuration.config.lang.parser.FileReader;
import com.alesharik.webserver.configuration.config.lang.parser.elements.ArrayImpl;
import com.alesharik.webserver.configuration.config.lang.parser.elements.ObjectImpl;
import com.alesharik.webserver.configuration.config.lang.parser.elements.TypedObjectImpl;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

//TODO grid not wroking on big imagrs
//TODO cpb - 5q images not showing
public class Cfg extends ConfigurationParser {
    public Cfg(@Nonnull File endpoint, @Nonnull FileReader fileReader) {
        super(endpoint, fileReader);
    }

    @Override
    public ConfigurationEndpoint parse() {
        return new ConfigurationEndpoint() {
            @Override
            public List<ConfigurationModule> getModules() {
                return Collections.emptyList();
            }

            @Override
            public String getName() {
                return "a";
            }

            @Override
            public CustomEndpointSection getCustomSection(String name) {
                if(name.equals("modules"))
                    return () -> Arrays.asList(new CustomEndpointSection.UseDirective() {
                                                   @Override
                                                   public String getName() {
                                                       return "l";
                                                   }

                                                   @Override
                                                   public ConfigurationTypedObject getConfiguration() {
                                                       TypedObjectImpl typedObject = new TypedObjectImpl("l", "litgame");
                                                       ObjectImpl object = new ObjectImpl("database");
                                                       object.getEntries().put("address", new ConfigurationPrimitive.String() {
                                                           @Override
                                                           public java.lang.String value() {
                                                               return "jdbc:postgresql://localhost:5432/postgres";
                                                           }

                                                           @Override
                                                           public java.lang.String getName() {
                                                               return "address";
                                                           }
                                                       });
                                                       object.getEntries().put("schema", new ConfigurationPrimitive.String() {
                                                           @Override
                                                           public java.lang.String value() {
                                                               return "litgame_prod";
                                                           }

                                                           @Override
                                                           public java.lang.String getName() {
                                                               return "schema";
                                                           }
                                                       });
                                                       object.getEntries().put("user", new ConfigurationPrimitive.String() {
                                                           @Override
                                                           public java.lang.String value() {
                                                               return "postgres";
                                                           }

                                                           @Override
                                                           public java.lang.String getName() {
                                                               return "user";
                                                           }
                                                       });
                                                       object.getEntries().put("password", new ConfigurationPrimitive.String() {
                                                           @Override
                                                           public java.lang.String value() {
                                                               return "test";
                                                           }

                                                           @Override
                                                           public java.lang.String getName() {
                                                               return "password";
                                                           }
                                                       });
                                                       typedObject.getEntries().put("database", object);
                                                       typedObject.getEntries().put("http-server", new ConfigurationPrimitive.String() {
                                                           @Override
                                                           public java.lang.String value() {
                                                               return "h";
                                                           }

                                                           @Override
                                                           public java.lang.String getName() {
                                                               return "http-server";
                                                           }
                                                       });
                                                       return typedObject;
                                                   }

                                                   @Override
                                                   public List<CustomEndpointSection.CustomProperty> getCustomProperties() {
                                                       return Collections.emptyList();
                                                   }
                                               },
                            new CustomEndpointSection.UseDirective() {
                                @Override
                                public String getName() {
                                    return "h";
                                }

                                @Override
                                public ConfigurationTypedObject getConfiguration() {
                                    TypedObjectImpl object = new TypedObjectImpl("server", "http-server");
                                    {
                                        ObjectImpl o = new ObjectImpl("pool");
                                        o.getEntries().put("name", new ConfigurationPrimitive.String() {
                                            @Override
                                            public java.lang.String value() {
                                                return "separated-executor-pool";
                                            }

                                            @Override
                                            public java.lang.String getName() {
                                                return "name";
                                            }
                                        });
                                        object.getEntries().put("pool", o);
                                    }
                                    {
                                        ArrayImpl array = new ArrayImpl("wrappers");
                                        object.getEntries().put("wrappers", array);
                                        ObjectImpl o = new ObjectImpl("");
                                        array.append(o);
                                        o.getEntries().put("name", new ConfigurationPrimitive.String() {
                                            @Override
                                            public java.lang.String value() {
                                                return "network-listener";
                                            }

                                            @Override
                                            public java.lang.String getName() {
                                                return "name";
                                            }
                                        });
                                        ObjectImpl o1 = new ObjectImpl("configuration");
                                        o.getEntries().put("configuration", o1);
                                        o1.getEntries().put("port", new ConfigurationPrimitive.Int() {
                                            @Override
                                            public int value() {
                                                return 7000;
                                            }

                                            @Override
                                            public java.lang.String getName() {
                                                return "port";
                                            }
                                        });
                                        o1.getEntries().put("host", new ConfigurationPrimitive.String() {
                                            @Override
                                            public java.lang.String value() {
                                                return "0.0.0.0";
                                            }

                                            @Override
                                            public java.lang.String getName() {
                                                return "host";
                                            }
                                        });
                                    }
                                    {
                                        ArrayImpl o = new ArrayImpl("bundles");
                                        object.getEntries().put("bundles", o);
                                    }
                                    object.getEntries().put("handler", new ConfigurationPrimitive.String() {
                                        @Override
                                        public java.lang.String value() {
                                            return "default";
                                        }

                                        @Override
                                        public java.lang.String getName() {
                                            return "handler";
                                        }
                                    });
                                    ArrayImpl addons = new ArrayImpl("addons");
                                    addons.append(new ConfigurationPrimitive.String() {
                                        @Override
                                        public java.lang.String value() {
                                            return "websocket";
                                        }

                                        @Override
                                        public java.lang.String getName() {
                                            return "websocket";
                                        }
                                    });
                                    object.getEntries().put("addons", addons);
                                    return object;
                                }

                                @Override
                                public List<CustomEndpointSection.CustomProperty> getCustomProperties() {
                                    return Collections.emptyList();
                                }
                            });
                return null;
            }

            @Override
            public ApiEndpointSection getApiSection() {
                ObjectImpl object = new ObjectImpl("");
                ObjectImpl o = new ObjectImpl("logger");
                object.getEntries().put("logger", o);
                o.getEntries().put("debug", new ConfigurationPrimitive.Boolean() {
                    @Override
                    public boolean value() {
                        return true;
                    }

                    @Override
                    public java.lang.String getName() {
                        return "debug";
                    }
                });
                o.getEntries().put("file", new ConfigurationPrimitive.String() {
                    @Override
                    public java.lang.String value() {
                        return "/tmp/a.log" + ThreadLocalRandom.current().nextInt();
                    }

                    @Override
                    public java.lang.String getName() {
                        return "file";
                    }
                });
                return new ApiEndpointSection() {
                    @Nullable
                    @Override
                    public ConfigurationElement getElement(String name) {
                        return object.getElement(name);
                    }

                    @Nullable
                    @Override
                    public <V extends ConfigurationElement> V getElement(String name, Class<V> clazz) {
                        return object.getElement(name, clazz);
                    }

                    @Override
                    public Set<String> getNames() {
                        return object.getNames();
                    }

                    @Override
                    public Map<String, ConfigurationElement> getEntries() {
                        return object.getEntries();
                    }

                    @Override
                    public int getSize() {
                        return object.getSize();
                    }

                    @Override
                    public boolean hasKey(String name) {
                        return object.hasKey(name);
                    }
                };
            }

            @Override
            public ScriptEndpointSection getScriptSection() {
                return name -> {
                    if(name.equals("init"))
                        return () -> Arrays.asList(new ScriptEndpointSection.Command() {
                                                       @Override
                                                       public String getName() {
                                                           return "start";
                                                       }

                                                       @Override
                                                       public ConfigurationElement getArg() {
                                                           return new ConfigurationPrimitive.String() {
                                                               @Override
                                                               public java.lang.String value() {
                                                                   return "l";
                                                               }

                                                               @Override
                                                               public java.lang.String getName() {
                                                                   return "";
                                                               }
                                                           };
                                                       }
                                                   },
                                new ScriptEndpointSection.Command() {

                                    @Override
                                    public String getName() {
                                        return "start";
                                    }

                                    @Override
                                    public ConfigurationElement getArg() {
                                        return new ConfigurationPrimitive.String() {
                                            @Override
                                            public java.lang.String value() {
                                                return "h";
                                            }

                                            @Override
                                            public java.lang.String getName() {
                                                return "";
                                            }
                                        };
                                    }
                                });
                    return null;
                };
            }

            @Override
            public List<ExternalLanguageHelper> getHelpers() {
                return Collections.emptyList();
            }
        };
    }
}
