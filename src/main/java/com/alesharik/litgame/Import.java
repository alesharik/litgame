package com.alesharik.litgame;

import com.alesharik.database.Database;
import com.alesharik.database.connection.FixedThreadBoundConnectionPool;
import com.alesharik.database.data.Schema;
import com.alesharik.database.driver.postgres.PostgresDriver;
import com.alesharik.litgame.data.DataManager;
import com.alesharik.litgame.data.Image;
import com.alesharik.litgame.data.ImageData;
import com.alesharik.litgame.data.Theme;
import com.alesharik.litgame.data.ThemeBlock;
import com.alesharik.webserver.api.agent.Agent;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

public class Import {
    public static void main(String[] args) throws IOException {
        while(Agent.isScanning()) {
            //nop
        }
        Database database = Database.newDatabase(new FixedThreadBoundConnectionPool("jdbc:postgresql://localhost:5432/postgres", "postgres", "ale456", 10),
                new PostgresDriver(),
                true);
        database.init();
        Schema litgame_prod = database.getSchema("litgame_prod", false);
        DataManager dataManager = new DataManager(litgame_prod, database.getTransactionManager());
        byte[] bytes = Files.readAllBytes(new File("/home/alexey/tmp/litgame/o/map.json").toPath());
        JsonArray parse = new JsonParser().parse(new String(bytes, StandardCharsets.UTF_8)).getAsJsonArray();
        dataManager.getTransactionManager().executeTransaction(() -> {
            for(JsonElement jsonElement : parse) {
                JsonObject asJsonObject = jsonElement.getAsJsonObject();
                ThemeBlock themeBlock = ThemeBlock.create(dataManager.getThemeBlockTable(), asJsonObject.get("name").getAsString());
                Theme theme = Theme.create(dataManager.getThemeTable(), "null", themeBlock);
                for(JsonElement images : asJsonObject.get("images").getAsJsonArray()) {
                    JsonObject o = images.getAsJsonObject();

                    File f = null;
                    for(File file : new File("/home/alexey/tmp/litgame/o/").listFiles()) {
                        if(file.getName().startsWith(o.get("id").getAsString())) {
                            f = file;
                            break;
                        }
                    }
                    if(f == null) {
                        System.out.println("Can't import " + o.get("name").getAsString());
                    } else {

                        Image image = Image.create(dataManager.getImageTable(), o.get("name").getAsString(), theme, false, false);
                        ImageData.create(dataManager.getImageDataTable(), image, Files.readAllBytes(f.toPath()));
                    }
                }
            }
            return new Object();
        });
        database.close();
    }
}
