package com.alesharik.litgame.data;

import com.alesharik.database.data.Schema;
import com.alesharik.database.data.Table;
import com.alesharik.database.transaction.TransactionManager;
import lombok.Getter;

public final class DataManager {
    public static final String THEME_TABLE_NAME = "themes";
    public static final String IMAGE_TABLE_NAME = "images";
    public static final String IMAGE_DATA_TABLE_NAME = "image_data";
    public static final String THEME_BLOCK_TABLE_NAME = "theme_blocks";

    @Getter
    private final Table<Theme> themeTable;
    @Getter
    private final Table<Image> imageTable;
    @Getter
    private final Table<ImageData> imageDataTable;
    @Getter
    private final Table<ThemeBlock> themeBlockTable;
    @Getter
    private final TransactionManager transactionManager;
    private final Schema schema;

    public DataManager(Schema schema, TransactionManager transactionManager) {
        this.schema = schema;
        this.transactionManager = transactionManager;
        Table<ThemeBlock> themeBlockTable = transactionManager.executeTransaction(() -> schema.getTable(THEME_BLOCK_TABLE_NAME, true, ThemeBlock.class));
        if(themeBlockTable == null)
            throw new IllegalStateException("Cannot create/get theme block table!");
        this.themeBlockTable = themeBlockTable;
        Table<Theme> themeTable = transactionManager.executeTransaction(() -> schema.getTable(THEME_TABLE_NAME, true, Theme.class));
        if(themeTable == null)
            throw new IllegalStateException("Cannot create/get theme table!");
        this.themeTable = themeTable;
        Table<Image> imageTable = transactionManager.executeTransaction(() -> schema.getTable(IMAGE_TABLE_NAME, true, Image.class));
        if(imageTable == null)
            throw new IllegalStateException("Cannot create/get image table!");
        this.imageTable = imageTable;
        Table<ImageData> imageDataTable = transactionManager.executeTransaction(() -> schema.getTable(IMAGE_DATA_TABLE_NAME, true, ImageData.class));
        if(imageDataTable == null)
            throw new IllegalStateException("Cannot create/get image data table!");
        this.imageDataTable = imageDataTable;
    }

    public String getSchema() {
        return schema.getName();
    }
}
