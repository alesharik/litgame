package com.alesharik.litgame.data;

import com.alesharik.database.entity.Column;
import com.alesharik.database.entity.Creator;
import com.alesharik.database.entity.Destroyer;
import com.alesharik.database.entity.Entity;
import com.alesharik.database.entity.EntityManager;
import com.alesharik.database.entity.PrimaryKey;
import com.alesharik.database.entity.Unique;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

@Entity
@EqualsAndHashCode
@ToString
@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
public final class ThemeBlock {
    @Column("id")
    @PrimaryKey
    public final UUID id;

    @Column("name")
    @Unique
    public String name;

    @Creator
    public static ThemeBlock create(EntityManager<ThemeBlock> entityManager, String name) {
        return new ThemeBlock(UUID.randomUUID(), name);
    }

    @Destroyer
    public void delete() {
    }
}
