package com.alesharik.litgame.data;

import com.alesharik.database.entity.Column;
import com.alesharik.database.entity.Creator;
import com.alesharik.database.entity.Destroyer;
import com.alesharik.database.entity.Entity;
import com.alesharik.database.entity.EntityManager;
import com.alesharik.database.entity.ForeignKey;
import com.alesharik.database.entity.PrimaryKey;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

@Entity
@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@RequiredArgsConstructor
public final class Image {
    @Column("id")
    @PrimaryKey
    public final UUID id;

    @Column("theme")
    @ForeignKey(value = DataManager.THEME_TABLE_NAME, onDelete = ForeignKey.Action.CASCADE)
    public UUID theme;

    @Column("is_answer")
    public boolean answer;

    @Column("name")
    public String name;

    @Column("is_question")
    public boolean question;

    @Creator
    public static Image create(EntityManager<Image> entityManager, String name, Theme theme, boolean isAnswer, boolean isQuestion) {
        return new Image(UUID.randomUUID(), theme.getId(), isAnswer, name, isQuestion);
    }

    @Destroyer
    public void delete() {
    }
}
