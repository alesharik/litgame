package com.alesharik.litgame.data;

import com.alesharik.database.entity.Column;
import com.alesharik.database.entity.Creator;
import com.alesharik.database.entity.Destroyer;
import com.alesharik.database.entity.Entity;
import com.alesharik.database.entity.EntityManager;
import com.alesharik.database.entity.ForeignKey;
import com.alesharik.database.entity.PrimaryKey;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

@Entity
@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public final class Theme {
    @Column("id")
    @PrimaryKey
    public final UUID id;

    @Column("name")
    public String name;

    @Column("block")
    @ForeignKey(value = DataManager.THEME_BLOCK_TABLE_NAME, onDelete = ForeignKey.Action.CASCADE)
    public UUID block;

    @Creator
    public static Theme create(EntityManager<Theme> entityManager, String name, ThemeBlock themeBlock) {
        return new Theme(UUID.randomUUID(), name, themeBlock.getId());
    }

    @Destroyer
    public void delete() {
    }
}
