package com.alesharik.litgame.data;

import com.alesharik.database.entity.Column;
import com.alesharik.database.entity.Creator;
import com.alesharik.database.entity.Destroyer;
import com.alesharik.database.entity.Entity;
import com.alesharik.database.entity.EntityManager;
import com.alesharik.database.entity.ForeignKey;
import com.alesharik.database.entity.PrimaryKey;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.annotation.Nullable;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.UUID;

@Entity
@EqualsAndHashCode
@ToString
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
public final class ImageData {
    @Column("image_id")
    @ForeignKey(value = DataManager.IMAGE_TABLE_NAME, onDelete = ForeignKey.Action.CASCADE)
    @PrimaryKey
    private final UUID imageId;

    /**
     * JPEG
     */
    @Column("data")
    private byte[] data;

    @Creator
    public static ImageData create(EntityManager<ImageData> entityManager, Image image, byte[] data) {
        return new ImageData(image.getId(), data);
    }

    @Creator
    public static ImageData create(EntityManager<ImageData> entityManager, Image image, BufferedImage data) throws IOException {
        if(data.getType() != BufferedImage.TYPE_3BYTE_BGR)
            throw new IllegalArgumentException("Image type is not 3BYTE_BGR!");
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        ImageIO.write(data, "jpg", stream);
        return new ImageData(image.getId(), stream.toByteArray());
    }

    @Destroyer
    public void destroy() {
    }

    public void writeData(BufferedImage image) throws IOException {
        if(image.getType() != BufferedImage.TYPE_3BYTE_BGR)
            throw new IllegalArgumentException("Image type is not 3BYTE_BGR!");
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", stream);
        setData(stream.toByteArray());
    }

    @Nullable
    public BufferedImage convertToImage() throws IOException {
        return ImageIO.read(new ByteArrayInputStream(data));
    }
}
