package com.alesharik.litgame.http;

import com.alesharik.litgame.data.DataManager;
import com.alesharik.litgame.data.Theme;
import com.alesharik.litgame.data.ThemeBlock;
import com.alesharik.webserver.api.GsonUtils;
import com.alesharik.webserver.module.http.bundle.processor.HttpProcessor;
import com.alesharik.webserver.module.http.http.HttpStatus;
import com.alesharik.webserver.module.http.http.Method;
import com.alesharik.webserver.module.http.http.Request;
import com.alesharik.webserver.module.http.http.Response;
import com.alesharik.webserver.module.http.http.data.MimeType;
import lombok.RequiredArgsConstructor;

import javax.annotation.Nonnull;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;

@SuppressWarnings("Duplicates")
@RequiredArgsConstructor
public final class ThemeHttpProcessor implements HttpProcessor {
    private static final MimeType JSON_MIME_TYPE = MimeType.parseType("application/json");
    private final DataManager dataManager;

    @Override
    public void process(@Nonnull Request request, @Nonnull Response response) {
        if(request.getMethod() == Method.GET)
            processGetRequest(request, response);
        else if(request.getMethod() == Method.PUT)
            processPutRequest(request, response);
        else if(request.getMethod() == Method.DELETE)
            processDeleteRequest(request, response);
        else if(request.getMethod() == Method.PATCH)
            processPatchMethod(request, response);
        else if(request.getMethod() == Method.OPTIONS)
            response.respond(HttpStatus.OK_200);
        else
            response.respond(HttpStatus.METHOD_NOT_ALLOWED_405);
    }

    private void processPatchMethod(@Nonnull Request request, @Nonnull Response response) {
        UUID id = request.getParameter("id") == null ? null : UUID.fromString(request.getParameter("id"));
        String body = validateJsonBody(request, response);
        if(body == null)
            return;

        Theme theme = GsonUtils.getGson().fromJson(body, Theme.class);
        if(id == null) {
            id = theme.id;
            if(id == null) {
                response.respond(HttpStatus.BAD_REQUEST_400);
                return;
            }
        }
        Theme dbAccess = dataManager.getThemeTable().selectByPrimaryKey(new Theme(id));
        if(dbAccess == null) {
            response.respond(HttpStatus.NOT_FOUND_404);
            return;
        }

        if(theme.block != null && !theme.block.equals(dbAccess.getBlock())) {
            if(dataManager.getThemeBlockTable().selectByPrimaryKey(new ThemeBlock(theme.block)) == null) {
                response.respond(HttpStatus.BAD_REQUEST_400);
                response.getWriter().write("Theme block " + theme.block + " not found");
                return;
            }
        }

        boolean ok = dataManager.getTransactionManager().executeTransaction(() -> {
            if(theme.name != null && !theme.name.equals(dbAccess.getName()))
                dbAccess.setName(theme.name);
            if(theme.block != null && !theme.block.equals(dbAccess.getBlock()))
                dbAccess.setBlock(theme.block);
        });
        if(ok)
            response.respond(HttpStatus.ACCEPTED_202);
        else {
            response.respond(HttpStatus.INTERNAL_SERVER_ERROR_500);
            response.getWriter().write("Can't edit entity in database");
        }
    }

    private void processDeleteRequest(@Nonnull Request request, @Nonnull Response response) {
        String id = request.getParameter("id");
        if(id == null) {
            response.respond(HttpStatus.BAD_REQUEST_400);
            return;
        }

        UUID uuid = UUID.fromString(id);
        Theme theme = dataManager.getThemeTable().selectByPrimaryKey(new Theme(uuid));
        if(theme == null)
            response.respond(HttpStatus.NOT_FOUND_404);
        else {
            boolean ok = dataManager.getTransactionManager().executeTransaction(theme::delete);
            if(ok)
                response.respond(HttpStatus.OK_200);
            else {
                response.respond(HttpStatus.INTERNAL_SERVER_ERROR_500);
                response.getWriter().write("Can't delete entity from database");
            }
        }
    }

    private void processPutRequest(@Nonnull Request request, @Nonnull Response response) {
        String body = validateJsonBody(request, response);
        if(body == null)
            return;

        Theme th = GsonUtils.getGson().fromJson(body, Theme.class);
        if(th.name == null || th.block == null || th.id != null) {
            response.respond(HttpStatus.BAD_REQUEST_400);
            return;
        }

        ThemeBlock block = dataManager.getThemeBlockTable().selectByPrimaryKey(new ThemeBlock(th.block));
        if(block == null) {
            response.respond(HttpStatus.BAD_REQUEST_400);
            response.getWriter().write("Theme block " + th.block + " not found");
            return;
        }

        Theme theme = dataManager.getTransactionManager().executeTransaction(
                () -> Theme.create(dataManager.getThemeTable(), th.name, block)
        );
        if(theme == null) {
            response.respond(HttpStatus.INTERNAL_SERVER_ERROR_500);
            response.getWriter().write("Can't add entity to database");
        } else {
            String s = GsonUtils.getGson().toJson(theme);
            response.respond(HttpStatus.CREATED_201);
            response.setType(JSON_MIME_TYPE, StandardCharsets.UTF_8);
            response.getWriter().write(s);
        }
    }

    private void processGetRequest(@Nonnull Request request, @Nonnull Response response) {
        String out;
        if(request.getParameter("id") != null) {
            UUID id = UUID.fromString(request.getParameter("id"));
            Theme theme = dataManager.getThemeTable().selectByPrimaryKey(new Theme(id));
            out = GsonUtils.getGson().toJson(theme);
        } else {
            List<Theme> select = dataManager.getThemeTable().select(-1);
            out = GsonUtils.getGson().toJson(select);
        }
        response.respond(HttpStatus.OK_200);
        response.setType(JSON_MIME_TYPE, StandardCharsets.UTF_8);
        response.getWriter().write(out);
    }

    private String validateJsonBody(@Nonnull Request request, @Nonnull Response response) {
        String body = request.getBodyAsString();
        if(body.isEmpty()) {
            response.respond(HttpStatus.LENGTH_REQUIRED_411);
            return null;
        }
        if(!request.getContentType().getType().equals(JSON_MIME_TYPE)) {
            response.respond(HttpStatus.UNSUPPORTED_MEDIA_TYPE_415);
            return null;
        }
        return body;
    }
}
