package com.alesharik.litgame.http;

import com.alesharik.litgame.data.DataManager;
import com.alesharik.webserver.module.http.addon.MessageProcessor;
import com.alesharik.webserver.module.http.addon.MessageProcessorParameters;
import com.alesharik.webserver.module.http.bundle.ErrorHandler;
import com.alesharik.webserver.module.http.bundle.HttpHandlerBundle;
import com.alesharik.webserver.module.http.bundle.Validator;
import com.alesharik.webserver.module.http.bundle.impl.error.ComplexErrorHandler;
import com.alesharik.webserver.module.http.bundle.impl.error.impl.DefaultErrorPageProvider;
import com.alesharik.webserver.module.http.bundle.processor.HttpProcessor;
import com.alesharik.webserver.module.http.http.HttpStatus;
import lombok.Getter;

import javax.annotation.Nonnull;

import static com.alesharik.webserver.module.http.bundle.processor.impl.HttpChainProcessor.chain;
import static com.alesharik.webserver.module.http.bundle.processor.impl.HttpRouterProcessor.router;

public final class HttpBundle implements HttpHandlerBundle {
    private final ComplexErrorHandler errorHandler;
    @Getter
    private final HttpProcessor processor;
    private final WSProcessor wsProcessor = new WSProcessor();

    public HttpBundle(DataManager dataManager) {
        this.errorHandler = new ComplexErrorHandler();
        this.errorHandler.addProvider(new DefaultErrorPageProvider());
        this.processor = chain()
                .process(
                        router()
                                .path("/api/edit/block", new ThemeBlockHttpProcessor(dataManager))
                                .path("/api/edit/theme/select", new SelectThemeHttpProcessor(dataManager))
                                .path("/api/edit/theme", new ThemeHttpProcessor(dataManager))
                                .path("/api/edit/image/data", new ImageDataHttpProcessor(dataManager))
                                .path("/api/edit/image/select", new SelectImageHttpProcessor(dataManager))
                                .path("/api/edit/image", new ImageHttpProcessor(dataManager))
                                .path("/api/control/block", new ThemeBlockHttpProcessor(dataManager))
                                .path("/api/control/theme", new SelectThemeHttpProcessor(dataManager))
                                .path("/api/control/image/data", new ImageDataHttpProcessor(dataManager))
                                .path("/api/control/image", new SelectImageHttpProcessor(dataManager))
//                                .path("/api/control/websocket/view", new WebSocketRequestUpgrader())
//                                .path("/api/control/websocket/controller", new WebSocketRequestUpgrader())
                                .defaultPath((request, response) -> response.respond(HttpStatus.NOT_FOUND_404))
                )
                .then(errorHandler.getErrorWrapper())
                .onError(errorHandler.getHttpErrorHandler());
    }

    @Nonnull
    @Override
    public Validator getValidator() {
        return request -> true;
    }

    @Nonnull
    @Override
    public ErrorHandler getErrorHandler() {
        return errorHandler.getErrorHandler();
    }

    @Override
    public MessageProcessor<?, ?, ?> getMessageProcessor(String name, MessageProcessorParameters parameters) {
        return wsProcessor;
    }
}
