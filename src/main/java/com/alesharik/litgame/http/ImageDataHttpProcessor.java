package com.alesharik.litgame.http;

import com.alesharik.litgame.data.DataManager;
import com.alesharik.litgame.data.Image;
import com.alesharik.litgame.data.ImageData;
import com.alesharik.webserver.module.http.bundle.processor.HttpProcessor;
import com.alesharik.webserver.module.http.http.HttpStatus;
import com.alesharik.webserver.module.http.http.Method;
import com.alesharik.webserver.module.http.http.Request;
import com.alesharik.webserver.module.http.http.Response;
import com.alesharik.webserver.module.http.http.data.MimeType;
import lombok.RequiredArgsConstructor;

import javax.annotation.Nonnull;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;

@SuppressWarnings("Duplicates")
@RequiredArgsConstructor
public final class ImageDataHttpProcessor implements HttpProcessor {
    private final DataManager dataManager;

    @Override
    public void process(@Nonnull Request request, @Nonnull Response response) {
        UUID id = UUID.fromString(request.getParameter("id"));
        if(request.getMethod() == Method.GET) {
            ImageData imageData = dataManager.getImageDataTable().selectByPrimaryKey(new ImageData(id));
            if(imageData == null) {
                response.respond(HttpStatus.NOT_FOUND_404);
                return;
            }
            byte[] data = imageData.getData();
            response.getOutputBuffer().write(data);
            response.setType(MimeType.parseType("image/jpeg"), null);
            response.respond(HttpStatus.OK_200);
        } else if(request.getMethod() == Method.PUT) {
            BufferedImage read;
            try {
                read = ImageIO.read(new ByteArrayInputStream(request.getBody()));
            } catch (IOException e) {
                e.printStackTrace();
                response.respond(HttpStatus.UNSUPPORTED_MEDIA_TYPE_415);
                return;
            }

            BufferedImage convert = new BufferedImage(read.getWidth(), read.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
            int[] data = read.getRGB(0, 0, read.getWidth(), read.getHeight(), null, 0, read.getWidth());
            convert.setRGB(0, 0, read.getWidth(), read.getHeight(), data, 0, read.getWidth());

            ImageData dat = dataManager.getImageDataTable().selectByPrimaryKey(new ImageData(id));
            if(dat == null) {
                Image image = dataManager.getImageTable().selectByPrimaryKey(new Image(id));
                if(image == null) {
                    response.respond(HttpStatus.NOT_FOUND_404);
                    return;
                } else {
                    String s = dataManager.getTransactionManager().executeTransaction(() -> {
                        ImageData.create(dataManager.getImageDataTable(), image, convert);
                        return "";
                    });
                    if(s == null) {
                        response.getWriter().write("Can't create an image data entity!");
                        response.respond(HttpStatus.INTERNAL_SERVER_ERROR_500);
                        return;
                    }
                }
            } else {
                String s = dataManager.getTransactionManager().executeTransaction(() -> {
                    dat.writeData(convert);
                    return "";
                });
                if(s == null) {
                    response.getWriter().write("Can't update an image data entity!");
                    response.respond(HttpStatus.INTERNAL_SERVER_ERROR_500);
                    return;
                }
            }
            response.respond(HttpStatus.OK_200);
        } else if(request.getMethod() == Method.OPTIONS)
            response.respond(HttpStatus.OK_200);
        else
            response.respond(HttpStatus.METHOD_NOT_ALLOWED_405);
    }
}
