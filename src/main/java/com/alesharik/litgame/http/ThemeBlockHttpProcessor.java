package com.alesharik.litgame.http;

import com.alesharik.litgame.data.DataManager;
import com.alesharik.litgame.data.ThemeBlock;
import com.alesharik.webserver.api.GsonUtils;
import com.alesharik.webserver.module.http.bundle.processor.HttpProcessor;
import com.alesharik.webserver.module.http.http.HttpStatus;
import com.alesharik.webserver.module.http.http.Method;
import com.alesharik.webserver.module.http.http.Request;
import com.alesharik.webserver.module.http.http.Response;
import com.alesharik.webserver.module.http.http.data.MimeType;
import lombok.RequiredArgsConstructor;

import javax.annotation.Nonnull;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;

@SuppressWarnings("Duplicates")
@RequiredArgsConstructor
public final class ThemeBlockHttpProcessor implements HttpProcessor {
    private static final MimeType JSON_MIME_TYPE = MimeType.parseType("application/json");
    private final DataManager dataManager;

    @Override
    public void process(@Nonnull Request request, @Nonnull Response response) {
        if(request.getMethod() == Method.GET)
            processGetRequest(request, response);
        else if(request.getMethod() == Method.PUT)
            processPutRequest(request, response);
        else if(request.getMethod() == Method.DELETE)
            processDeleteRequest(request, response);
        else if(request.getMethod() == Method.PATCH)
            processPatchMethod(request, response);

        else if(request.getMethod() == Method.OPTIONS)
            response.respond(HttpStatus.OK_200);
        else
            response.respond(HttpStatus.METHOD_NOT_ALLOWED_405);
    }

    private void processPatchMethod(@Nonnull Request request, @Nonnull Response response) {
        UUID id = request.getParameter("id") == null ? null : UUID.fromString(request.getParameter("id"));
        String body = validateJsonBody(request, response);
        if(body == null)
            return;

        ThemeBlock themeBlock = GsonUtils.getGson().fromJson(body, ThemeBlock.class);
        if(id == null) {
            id = themeBlock.id;
            if(id == null) {
                response.respond(HttpStatus.BAD_REQUEST_400);
                return;
            }
        }
        ThemeBlock dbAccess = dataManager.getThemeBlockTable().selectByPrimaryKey(new ThemeBlock(id));
        if(dbAccess == null) {
            response.respond(HttpStatus.NOT_FOUND_404);
            return;
        }

        boolean ok = dataManager.getTransactionManager().executeTransaction(() -> {
            if(themeBlock.name != null && !themeBlock.name.equals(dbAccess.getName()))
                dbAccess.setName(themeBlock.name);
        });
        if(ok)
            response.respond(HttpStatus.ACCEPTED_202);
        else {
            response.respond(HttpStatus.INTERNAL_SERVER_ERROR_500);
            response.getWriter().write("Can't edit entity in database");
        }
    }

    private void processDeleteRequest(@Nonnull Request request, @Nonnull Response response) {
        String id = request.getParameter("id");
        if(id == null) {
            response.respond(HttpStatus.BAD_REQUEST_400);
            return;
        }

        UUID uuid = UUID.fromString(id);
        ThemeBlock themeBlock = dataManager.getThemeBlockTable().selectByPrimaryKey(new ThemeBlock(uuid));
        if(themeBlock == null)
            response.respond(HttpStatus.NOT_FOUND_404);
        else {
            boolean ok = dataManager.getTransactionManager().executeTransaction(themeBlock::delete);
            if(ok)
                response.respond(HttpStatus.OK_200);
            else {
                response.respond(HttpStatus.INTERNAL_SERVER_ERROR_500);
                response.getWriter().write("Can't delete entity from database");
            }
        }
    }

    private void processPutRequest(@Nonnull Request request, @Nonnull Response response) {
        String body = validateJsonBody(request, response);
        if(body == null)
            return;

        ThemeBlock themeBlock = GsonUtils.getGson().fromJson(body, ThemeBlock.class);
        if(themeBlock.name == null || themeBlock.id != null) {
            response.respond(HttpStatus.BAD_REQUEST_400);
            return;
        }

        ThemeBlock block = dataManager.getTransactionManager().executeTransaction(
                () -> ThemeBlock.create(dataManager.getThemeBlockTable(), themeBlock.name)
        );
        if(block == null) {
            response.respond(HttpStatus.INTERNAL_SERVER_ERROR_500);
            response.getWriter().write("Can't add entity to database");
        } else {
            String s = GsonUtils.getGson().toJson(block);
            response.respond(HttpStatus.CREATED_201);
            response.setType(JSON_MIME_TYPE, StandardCharsets.UTF_8);
            response.getWriter().write(s);
        }
    }

    private void processGetRequest(@Nonnull Request request, @Nonnull Response response) {
        String out;
        if(request.getParameter("id") != null) {
            UUID id = UUID.fromString(request.getParameter("id"));
            ThemeBlock themeBlock = dataManager.getThemeBlockTable().selectByPrimaryKey(new ThemeBlock(id));
            out = GsonUtils.getGson().toJson(themeBlock);
        } else {
            List<ThemeBlock> select = dataManager.getThemeBlockTable().select(-1);
            out = GsonUtils.getGson().toJson(select);
        }
        response.respond(HttpStatus.OK_200);
        response.setType(JSON_MIME_TYPE, StandardCharsets.UTF_8);
        response.getWriter().write(out);
    }

    private String validateJsonBody(@Nonnull Request request, @Nonnull Response response) {
        String body = request.getBodyAsString();
        if(body.isEmpty()) {
            response.respond(HttpStatus.LENGTH_REQUIRED_411);
            return null;
        }
        if(!request.getContentType().getType().equals(JSON_MIME_TYPE)) {
            response.respond(HttpStatus.UNSUPPORTED_MEDIA_TYPE_415);
            return null;
        }
        return body;
    }
}
