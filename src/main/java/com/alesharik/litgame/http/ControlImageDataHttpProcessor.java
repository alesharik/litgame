package com.alesharik.litgame.http;

import com.alesharik.litgame.data.DataManager;
import com.alesharik.litgame.data.ImageData;
import com.alesharik.webserver.module.http.bundle.processor.HttpProcessor;
import com.alesharik.webserver.module.http.http.HttpStatus;
import com.alesharik.webserver.module.http.http.Method;
import com.alesharik.webserver.module.http.http.Request;
import com.alesharik.webserver.module.http.http.Response;
import com.alesharik.webserver.module.http.http.data.MimeType;
import lombok.RequiredArgsConstructor;

import javax.annotation.Nonnull;
import java.util.UUID;

@SuppressWarnings("Duplicates")
@RequiredArgsConstructor
public final class ControlImageDataHttpProcessor implements HttpProcessor {
    private final DataManager dataManager;

    @Override
    public void process(@Nonnull Request request, @Nonnull Response response) {
        if(request.getMethod() == Method.GET) {
            ImageData imageData = dataManager.getImageDataTable().selectByPrimaryKey(new ImageData(UUID.fromString(request.getParameter("id"))));
            if(imageData == null) {
                response.respond(HttpStatus.NOT_FOUND_404);
                return;
            }
            byte[] data = imageData.getData();
            response.getOutputBuffer().write(data);
            response.setType(MimeType.parseType("image/jpeg"), null);
            response.respond(HttpStatus.OK_200);
        } else if(request.getMethod() == Method.OPTIONS)
            response.respond(HttpStatus.OK_200);
        else {
            response.respond(HttpStatus.METHOD_NOT_ALLOWED_405);
        }
    }
}
