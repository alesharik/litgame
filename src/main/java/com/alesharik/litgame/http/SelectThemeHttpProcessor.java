package com.alesharik.litgame.http;

import com.alesharik.database.data.EntityPreparedStatement;
import com.alesharik.litgame.data.DataManager;
import com.alesharik.litgame.data.Theme;
import com.alesharik.webserver.api.GsonUtils;
import com.alesharik.webserver.module.http.bundle.processor.HttpProcessor;
import com.alesharik.webserver.module.http.http.HttpStatus;
import com.alesharik.webserver.module.http.http.Method;
import com.alesharik.webserver.module.http.http.Request;
import com.alesharik.webserver.module.http.http.Response;
import com.alesharik.webserver.module.http.http.data.MimeType;
import lombok.RequiredArgsConstructor;

import javax.annotation.Nonnull;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@SuppressWarnings("Duplicates")
@RequiredArgsConstructor
public final class SelectThemeHttpProcessor implements HttpProcessor {
    private static final MimeType JSON_MIME_TYPE = MimeType.parseType("application/json");
    private final DataManager dataManager;

    @Override
    public void process(@Nonnull Request request, @Nonnull Response response) {
        if(request.getMethod() != Method.GET) {
            response.respond(HttpStatus.METHOD_NOT_ALLOWED_405);
            return;
        }
        if(request.getMethod() == Method.OPTIONS) {
            response.respond(HttpStatus.OK_200);
            return;
        }
        String blockId = request.getParameter("block_id");
        if(blockId != null) {
            UUID id = UUID.fromString(blockId);
            EntityPreparedStatement<Theme> st = dataManager.getThemeTable().prepareStatement("SELECT * FROM " + dataManager.getSchema() + '.' + DataManager.THEME_TABLE_NAME + " WHERE block = ?");
            try {
                st.setObject(1, id);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            List<Theme> themes = st.executeEntityQuery();
            String out = GsonUtils.getGson().toJson(themes);
            response.setType(JSON_MIME_TYPE, StandardCharsets.UTF_8);
            response.getWriter().write(out);
            response.respond(HttpStatus.OK_200);
        }
    }
}
