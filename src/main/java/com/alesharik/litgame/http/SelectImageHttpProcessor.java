package com.alesharik.litgame.http;

import com.alesharik.database.data.EntityPreparedStatement;
import com.alesharik.litgame.data.DataManager;
import com.alesharik.litgame.data.Image;
import com.alesharik.webserver.api.GsonUtils;
import com.alesharik.webserver.module.http.bundle.processor.HttpProcessor;
import com.alesharik.webserver.module.http.http.HttpStatus;
import com.alesharik.webserver.module.http.http.Method;
import com.alesharik.webserver.module.http.http.Request;
import com.alesharik.webserver.module.http.http.Response;
import com.alesharik.webserver.module.http.http.data.MimeType;
import lombok.RequiredArgsConstructor;

import javax.annotation.Nonnull;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@SuppressWarnings("Duplicates")
@RequiredArgsConstructor
public final class SelectImageHttpProcessor implements HttpProcessor {
    private static final MimeType JSON_MIME_TYPE = MimeType.parseType("application/json");
    private final DataManager dataManager;

    @Override
    public void process(@Nonnull Request request, @Nonnull Response response) {
        if(request.getMethod() != Method.GET) {
            response.respond(HttpStatus.OK_200);
            return;
        }

        if(request.getMethod() == Method.OPTIONS) {
            response.respond(HttpStatus.OK_200);
            return;
        }
        String themeId = request.getParameter("theme_id");
        if(themeId != null) {
            UUID id = UUID.fromString(themeId);
            EntityPreparedStatement<Image> th = dataManager.getImageTable().prepareStatement("SELECT * FROM " + dataManager.getSchema() + '.' + DataManager.IMAGE_TABLE_NAME + " WHERE theme = ?");
            try {
                th.setObject(1, id);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            List<Image> images = th.executeEntityQuery();
            String s = GsonUtils.getGson().toJson(images);
            response.setType(JSON_MIME_TYPE, StandardCharsets.UTF_8);
            response.getWriter().write(s);
            response.respond(HttpStatus.OK_200);
        }
    }
}
