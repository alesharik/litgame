package com.alesharik.litgame.http;

import com.alesharik.litgame.data.DataManager;
import com.alesharik.litgame.data.Theme;
import com.alesharik.webserver.api.GsonUtils;
import com.alesharik.webserver.module.http.bundle.processor.HttpProcessor;
import com.alesharik.webserver.module.http.http.HttpStatus;
import com.alesharik.webserver.module.http.http.Request;
import com.alesharik.webserver.module.http.http.Response;
import com.alesharik.webserver.module.http.http.data.MimeType;
import lombok.RequiredArgsConstructor;

import javax.annotation.Nonnull;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

@RequiredArgsConstructor
public final class ControlThemeHttpProcessor implements HttpProcessor {
    private static final MimeType JSON_MIME_TYPE = MimeType.parseType("application/json");
    private final DataManager dataManager;

    @Override
    public void process(@Nonnull Request request, @Nonnull Response response) {
        String out;
        UUID id = UUID.fromString(request.getParameter("id"));
        Theme theme = dataManager.getThemeTable().selectByPrimaryKey(new Theme(id));
        out = GsonUtils.getGson().toJson(theme);

        response.respond(HttpStatus.OK_200);
        response.setType(JSON_MIME_TYPE, StandardCharsets.UTF_8);
        response.getWriter().write(out);
    }
}
