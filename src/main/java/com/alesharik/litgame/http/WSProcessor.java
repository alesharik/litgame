package com.alesharik.litgame.http;

import com.alesharik.webserver.module.http.addon.websocket.impl.WebSocketBroadcaster;
import com.alesharik.webserver.module.http.addon.websocket.processor.WebSocketMessageProcessorContext;
import com.alesharik.webserver.module.http.addon.websocket.processor.WiredWebSocketMessageProcessor;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public final class WSProcessor extends WiredWebSocketMessageProcessor {
    private final List<WebSocketBroadcaster> views = new CopyOnWriteArrayList<>();

    @Override
    public void onConnect(WebSocketMessageProcessorContext context, WebSocketBroadcaster broadcaster) {
        if(context.getHandshakeRequest().getContextPath().endsWith("view"))
            views.add(broadcaster);
    }

    @Override
    public void onClose(WebSocketMessageProcessorContext context, WebSocketBroadcaster broadcaster, int code) {
        views.remove(broadcaster);
    }

    @Override
    public void onMessage(WebSocketMessageProcessorContext context, WebSocketBroadcaster broadcaster, String message) {
        for(WebSocketBroadcaster view : views) {
            view.sendMessage(message);
        }
    }
}
