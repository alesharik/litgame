package com.alesharik.litgame.http;

import com.alesharik.litgame.data.DataManager;
import com.alesharik.litgame.data.Image;
import com.alesharik.litgame.data.Theme;
import com.alesharik.webserver.api.GsonUtils;
import com.alesharik.webserver.module.http.bundle.processor.HttpProcessor;
import com.alesharik.webserver.module.http.http.HttpStatus;
import com.alesharik.webserver.module.http.http.Method;
import com.alesharik.webserver.module.http.http.Request;
import com.alesharik.webserver.module.http.http.Response;
import com.alesharik.webserver.module.http.http.data.MimeType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.annotation.Nonnull;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;

@SuppressWarnings("Duplicates")
@RequiredArgsConstructor
public final class ImageHttpProcessor implements HttpProcessor {
    private static final MimeType JSON_MIME_TYPE = MimeType.parseType("application/json");
    private final DataManager dataManager;

    @Override
    public void process(@Nonnull Request request, @Nonnull Response response) {
        if(request.getMethod() == Method.GET)
            processGetRequest(request, response);
        else if(request.getMethod() == Method.PUT)
            processPutRequest(request, response);
        else if(request.getMethod() == Method.DELETE)
            processDeleteRequest(request, response);
        else if(request.getMethod() == Method.PATCH)
            processPatchMethod(request, response);
        else if(request.getMethod() == Method.OPTIONS)
            response.respond(HttpStatus.OK_200);
        else
            response.respond(HttpStatus.METHOD_NOT_ALLOWED_405);
    }

    private void processPatchMethod(@Nonnull Request request, @Nonnull Response response) {
        UUID id = request.getParameter("id") == null ? null : UUID.fromString(request.getParameter("id"));
        String body = validateJsonBody(request, response);
        if(body == null)
            return;

        Img image = GsonUtils.getGson().fromJson(body, Img.class);
        if(id == null) {
            id = image.id;
            if(id == null) {
                response.respond(HttpStatus.BAD_REQUEST_400);
                return;
            }
        }
        Image dbAccess = dataManager.getImageTable().selectByPrimaryKey(new Image(id));
        if(dbAccess == null) {
            response.respond(HttpStatus.NOT_FOUND_404);
            return;
        }

        if(image.theme != null && !image.theme.equals(dbAccess.getTheme())) {
            if(dataManager.getThemeTable().selectByPrimaryKey(new Theme(image.theme)) == null) {
                response.respond(HttpStatus.BAD_REQUEST_400);
                response.getWriter().write("Theme block " + image.theme + " not found");
                return;
            }
        }

        boolean ok = dataManager.getTransactionManager().executeTransaction(() -> {
            if(image.name != null && !image.name.equals(dbAccess.getName()))
                dbAccess.setName(image.name);
            if(image.theme != null && !image.theme.equals(dbAccess.getTheme()))
                dbAccess.setTheme(image.theme);
            if(image.answer != null && !image.answer.equals(dbAccess.isAnswer()))
                dbAccess.setAnswer(image.answer);
            if(image.question != null && !image.question.equals(dbAccess.isQuestion()))
                dbAccess.setQuestion(image.question);
        });
        if(ok)
            response.respond(HttpStatus.ACCEPTED_202);
        else {
            response.respond(HttpStatus.INTERNAL_SERVER_ERROR_500);
            response.getWriter().write("Can't edit entity in database");
        }
    }

    private void processDeleteRequest(@Nonnull Request request, @Nonnull Response response) {
        String id = request.getParameter("id");
        if(id == null) {
            response.respond(HttpStatus.BAD_REQUEST_400);
            return;
        }

        UUID uuid = UUID.fromString(id);
        Image image = dataManager.getImageTable().selectByPrimaryKey(new Image(uuid));
        if(image == null)
            response.respond(HttpStatus.NOT_FOUND_404);
        else {
            boolean ok = dataManager.getTransactionManager().executeTransaction(image::delete);
            if(ok)
                response.respond(HttpStatus.OK_200);
            else {
                response.respond(HttpStatus.INTERNAL_SERVER_ERROR_500);
                response.getWriter().write("Can't delete entity from database");
            }
        }
    }

    private void processPutRequest(@Nonnull Request request, @Nonnull Response response) {
        String body = validateJsonBody(request, response);
        if(body == null)
            return;

        Image image = GsonUtils.getGson().fromJson(body, Image.class);
        if(image.name == null || image.theme == null || image.id != null) {
            response.respond(HttpStatus.BAD_REQUEST_400);
            return;
        }

        Theme theme = dataManager.getThemeTable().selectByPrimaryKey(new Theme(image.theme));
        if(theme == null) {
            response.respond(HttpStatus.BAD_REQUEST_400);
            response.getWriter().write("Theme block " + image.theme + " not found");
            return;
        }

        Image block = dataManager.getTransactionManager().executeTransaction(
                () -> Image.create(dataManager.getImageTable(), image.name, theme, image.answer, image.question)
        );
        if(block == null) {
            response.respond(HttpStatus.INTERNAL_SERVER_ERROR_500);
            response.getWriter().write("Can't add entity to database");
        } else {
            String s = GsonUtils.getGson().toJson(block);
            response.respond(HttpStatus.CREATED_201);
            response.setType(JSON_MIME_TYPE, StandardCharsets.UTF_8);
            response.getWriter().write(s);
        }
    }

    private void processGetRequest(@Nonnull Request request, @Nonnull Response response) {
        String out;
        if(request.getParameter("id") != null) {
            UUID id = UUID.fromString(request.getParameter("id"));
            Image image = dataManager.getImageTable().selectByPrimaryKey(new Image(id));
            out = GsonUtils.getGson().toJson(image);
        } else {
            List<Image> select = dataManager.getImageTable().select(-1);
            out = GsonUtils.getGson().toJson(select);
        }
        response.respond(HttpStatus.OK_200);
        response.setType(JSON_MIME_TYPE, StandardCharsets.UTF_8);
        response.getWriter().write(out);
    }

    private String validateJsonBody(@Nonnull Request request, @Nonnull Response response) {
        String body = request.getBodyAsString();
        if(body.isEmpty()) {
            response.respond(HttpStatus.LENGTH_REQUIRED_411);
            return null;
        }
        if(!request.getContentType().getType().equals(JSON_MIME_TYPE)) {
            response.respond(HttpStatus.UNSUPPORTED_MEDIA_TYPE_415);
            return null;
        }
        return body;
    }

    @RequiredArgsConstructor
    @Getter
    @Setter
    @EqualsAndHashCode
    @ToString
    private static final class Img {
        private final UUID id;
        private final UUID theme;
        private final Boolean answer;
        private final String name;
        private final Boolean question;
    }
}
